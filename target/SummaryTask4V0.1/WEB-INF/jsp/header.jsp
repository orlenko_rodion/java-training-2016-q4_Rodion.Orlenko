<%@ page import="ua.nure.orlenko.SummaryTask4.db.service.entities.UserS" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <script src= "<c:url value="/styles/bootstrap/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/styles/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value="/styles/bootstrap/css/bootstrap.css"/>">

    <link rel="stylesheet" href="<c:url value="/styles/header.css"/>">

</head>

<body>
<%
    UserS user = (UserS) request.getSession().getAttribute("user");
    ResourceBundle bundle = (ResourceBundle) request.getSession().getAttribute("resourceBundle");
    if (bundle == null) {
        bundle = ResourceBundle.getBundle("Language", new Locale("en","US"));
    }
%>

    <div id="head">
        <div id="logo">
            <img src="src/books.png" style="width: 150px; height: 150px">
        </div>

        <div id="mainHead">
            <a href="<c:url value="/index.jsp"/>"><h1><%=bundle.getString("head.title")%></h1></a>
            <div id="mainHeadLang">
                <form action="/controller" method="post">
                    <input type="hidden" name="command" value="locale">
                    <input type="image" name="locale" value="en" src="<c:url value="/src/US_flag_51_stars.svg.png"/>" height="20" width="20" alt="radio"/>
                    <input type="image" name="locale" value="ru" src="<c:url value="/src/Flag_of_Russia.svg.png"/>" height="20" width="20" alt="radio"/>
                </form>
            </div>
        </div>

        <div id="userBlock">
            <center>
            <%if (user == null) {%>
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#loginModal"><%=bundle.getString("head.login")%></button>
                <%--LOGIN MODAL--%>
                <div id="loginModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button onclick="" class="close" type="button" data-dismiss="modal">×</button>
                                <label class="information"><%=bundle.getString("head.log/reg")%></label>
                            </div>


                            <div class="modal-body" >
                                <form id="login_form" action="/controller" method="post">
                                    <input type="hidden" name="command" value="login">

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input id="login" type="text" class="form-control" name="login" placeholder="<%=bundle.getString("head.login")%>" required>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="password" type="password" class="form-control" name="password" placeholder="<%=bundle.getString("head.password")%>" required>
                                    </div>
                                    <br>
                                    <input class="btn-success" type="submit" value="<%=bundle.getString("head.login")%>"/>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-block"data-dismiss="modal" data-toggle="modal" data-target="#registerModal"><%=bundle.getString("head.register")%></button>
                                <br>
                                <button class="btn-danger" type="button" data-dismiss="modal"><%=bundle.getString("head.cancel")%></button>
                            </div>

                        </div>
                    </div>
                </div>
            </center>
                <%--REGISTER MODAL--%>
                <div id="registerModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button onclick="" class="close" type="button" data-dismiss="modal">×</button>
                                <label class="information"><%=bundle.getString("head.registration")%></label>
                            </div>
                            <div class="modal-body">
                                <form id="register_form" action="/controller" method="post">
                                    <input type="hidden" name="command" value="register">
                                    <%--login--%>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" class="form-control" name="login" placeholder="<%=bundle.getString("head.login")%>" required>
                                    </div>
                                    <%--password--%>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" class="form-control" name="password" placeholder="<%=bundle.getString("head.password")%>" required>
                                    </div>
                                    <%--firstName--%>
                                    <div class="form-group">
                                        <label for="fstName"><%=bundle.getString("head.name")%>:</label>
                                        <input type="text" class="form-control" id="fstName" name="firstName" required>
                                    </div>
                                    <%--secondName--%>
                                    <div class="form-group">
                                        <label for="secName"><%=bundle.getString("head.surname")%>:</label>
                                        <input type="text" class="form-control" id="secName" name="secondName" required>
                                    </div>
                                    <%--gender--%>
                                    <div class="form-group">
                                        <label for="sel1"><%=bundle.getString("head.gender")%>:</label>
                                        <select class="form-control" id="sel1" name="gender">
                                            <option value="Male"><%=bundle.getString("head.gender.male")%></option>
                                            <option value="Female"><%=bundle.getString("head.gender.female")%></option>
                                        </select>
                                    </div>
                                    <%--birthDate--%>
                                        <label><%=bundle.getString("head.birthDate")%>:</label>
                                        <input type="date" name="birthDate">
                                    <%--lang--%>
                                    <div class="form-group">
                                        <label for="sel2"><%=bundle.getString("head.baseLang")%>:</label>
                                        <select class="form-control" id="sel2" name="lang">
                                            <option value="EN">EN</option>
                                            <option value="RU">RU</option>
                                        </select>
                                    </div>
                                    <button class="btn-success" type="submit"><%=bundle.getString("head.registration")%></button>
                                </form>

                            </div>
                            <div class="modal-footer">
                                    <button class="btn-danger" type="button" data-dismiss="modal"><%=bundle.getString("head.cancel")%></button>
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>
            <%if (user != null) {
            String firstName = user.getDefaultTranslate().getFirstName();
            String secondName = user.getDefaultTranslate().getSecondName();%>
            <form action="/controller" method="get">
                <input type="hidden" name="command" value="cabinet">
                <input class="btn" type="submit" name="name" value="<%=firstName%> <%=secondName%>" >
            </form>
            <br>
            <form action="/controller" method="get">
                <input type="hidden" name="command" value="logout">
                <input class="btn-link" type="submit" name="name" value="<%=bundle.getString("head.logout")%>" >
            </form>
            <%}%>


        </div>
    </div>





</body>
</html>
