
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <script src= "<c:url value="/styles/bootstrap/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/styles/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value="/styles/bootstrap/css/bootstrap.css"/>">

    <link rel="stylesheet" href="<c:url value="/styles/rightColumn.css"/>">
</head>

<body>
<center>
<label class="label-default"><%=bundle.getString("rightCol.menu")%></label>
</center>
<%-----------------------------------------getAllEditions--------------------------------%>
<form action="/controller" method="post">
    <input type="hidden" name="command" value="getAllEditions">
    <button class="btn-block" id="plug1" type="submit"><%=bundle.getString("rightCol.editions") %></button>
</form>

</body>
</html>
