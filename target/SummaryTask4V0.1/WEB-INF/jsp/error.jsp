<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title></title>

    <script src= "<c:url value="/styles/bootstrap/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/styles/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value="/styles/bootstrap/css/bootstrap.css"/>">

    <link rel="stylesheet" href="<c:url value="/styles/index.css"/>">
</head>
<%String errorMessage = (String) request.getAttribute("errorMessage");%>
<body>
<div id="indexContent">
    <div id="indexHeader">
        <%@ include file="header.jsp"%>
    </div>
    <div id="indexMainContent">
        <div id="indexCentralColumn">
        <label class="label"></label>
            <%if (errorMessage == null) {%>
                <label class="label-warning" ><%=bundle.getString("error.unknown")%> :(</label>
            <%} else {%>
                <label class="label-warning" ><%=errorMessage%></label>
            <%}%>


        </div>
        <div id="indexRightColumn">
            <%@ include file="/WEB-INF/jsp/rightColumn.jsp"%>
        </div>
    </div>
    <div id="indexFooter">

    </div>
</div>

</body>
</html>
