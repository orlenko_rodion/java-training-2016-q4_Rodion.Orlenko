package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserDAO;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.UserTranslate;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserTranslateDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.util.List;

/**
 * Test class
 */
public class UserTranslateDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public UserTranslateDaoIplTest() throws DBException {
    }

    @Test
    public void mainFunctionality() throws AppException {
        //createTest();
        //getByIdTest();
        //getByNameTest();
        updateTest();
        getAllTest();
    }
    private void createTest(){
        UserTranslateDAO userTranslateDAO = null;
        UserDAO userDAO = null;
        LanguageDAO languageDAO;

        User user = new User();
        Language language = null;
        UserTranslate userTranslate = null;

        try {
            userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
            languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
            userTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserTranslateDao();

            user = userDAO.get(2L,connection);
            language = languageDAO.get(Locale.EN.toString(),connection);
            userTranslate = new UserTranslate();
            userTranslate.setFirstName("Peter");
            userTranslate.setSecondName("Pupkin");
            userTranslate.setRef_lang(language.getIdlang());
            userTranslate.setRef_user(user.getIdUser());

            System.out.println(userTranslateDAO.create(userTranslate,connection));


        } catch (AppException e) {
            e.printStackTrace();
        }

    }
    private void getByIdTest() throws AppException {
        UserTranslateDAO userTranslateDAO = null;

        userTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserTranslateDao();
        UserTranslate user = userTranslateDAO.get(1L,connection);
        System.out.println(user);
    }
    private void getByNameTest() throws AppException {
        UserTranslateDAO userTranslateDAO = null;

        userTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserTranslateDao();
        UserTranslate user = userTranslateDAO.get("Peter","Pupkin",connection);
        System.out.println(user);
    }
    private void updateTest() throws AppException {
        UserTranslate userTranslate = null;
        UserTranslateDAO userTranslateDAO = null;
        userTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserTranslateDao();

        userTranslate = userTranslateDAO.get(1L,connection);
        userTranslate.setFirstName("Vasya");

        System.out.println(userTranslateDAO.update(userTranslate,connection));

    }
    private void getAllTest() throws AppException {
        UserTranslateDAO userTranslateDAO = null;

        userTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserTranslateDao();
        List<UserTranslate> list = userTranslateDAO.getAll(connection);
        System.out.println(list);
    }
}
