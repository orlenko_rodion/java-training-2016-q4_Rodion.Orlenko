package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Account;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.AccountDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test class
 */

public class AccountDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public AccountDaoIplTest() throws DBException {
    }


    @Test
    public void mainFunctionality() throws AppException {
        Account account = new Account("1234567899");
        assertEquals(account.getBalance(),new Double(0));

        createTest();
        updateTest();
        getAll();
        getByIdTest();
        deleteTest();
    }
    private void createTest() {
        Account account = new Account("1234567899");
        AccountDAO accountDAO = null;

        try {
            accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();
            accountDAO.create(account,connection);
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
    private void updateTest() throws AppException {
        Account account = new Account("1234567899",10D);
        AccountDAO accountDAO = null;

        accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();
        accountDAO.update(account,connection);

    }
    private void getAll() throws AppException {
        AccountDAO accountDAO = null;

        accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();
        List<Account> list = accountDAO.getAll(connection);
        System.out.println(list);
    }
    private void getByIdTest() throws AppException {
        AccountDAO accountDAO = null;

        accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();
        Account account = accountDAO.get("1234567899",connection);

        assertEquals(account.getBalance(),(Double)10D);
    }
    private void deleteTest() throws AppException {
        Account account = new Account("1234567899");
        AccountDAO accountDAO = null;

        accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();
        accountDAO.delete(account.getIdAccount(),connection);
    }
}
