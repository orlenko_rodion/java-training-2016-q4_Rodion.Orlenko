package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionDAO;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

/**
 * Test class
 */
public class EditionDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public EditionDaoIplTest() throws DBException {
    }

    @Test
    public void mainFunctionality() throws AppException {
        createTest();
        //getTest();
        //delete();
        //updateTest();
        //getAllTest();
    }
    private void createTest() {
        EditionDAO editionDAO = null;
        Edition edition = new Edition();

        try {
            editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();

            edition.setPrice(1000D);
            System.out.println(editionDAO.create(edition,connection));


        } catch (AppException e) {
            e.printStackTrace();
        }
    }
    private void updateTest() throws AppException {
        Edition edition = null;
        EditionDAO editionDAO = null;

        editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
        edition = editionDAO.get(4L,connection);
        edition.setPublicationDate(new Date(System.currentTimeMillis()));
        editionDAO.update(edition,connection);

    }
    private void getTest() throws AppException {
        EditionDAO editionDAO = null;

        editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
        Edition user = editionDAO.get(2L,connection);
        System.out.println(user);
    }
    private void delete() throws AppException {
        EditionDAO editionDAO = null;

        editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
        editionDAO.delete(4L,connection);
    }
    private void getAllTest() throws AppException {
        EditionDAO editionDAO = null;

        editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
        List<Edition> list = editionDAO.getAll(connection);
        System.out.println(list);
    }
}
