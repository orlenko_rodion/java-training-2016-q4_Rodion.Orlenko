package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.EditionTranslate;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionTranslateDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.util.List;

/**
 * Test class
 */
public class EditionTranslateDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public EditionTranslateDaoIplTest() throws DBException {
    }

    @Test
    public void mainFunctionality() throws AppException {
        //createTest();
        //updateTest();
        //getByNameTest();
        //getByIdTest();
        getAllTest();
        //deleteByTitleTest();
        //deleteByIdTest();
    }
    private void createTest(){
        EditionTranslateDAO editionTranslateDAO = null;
        EditionDAO editionDAO = null;
        LanguageDAO languageDAO;

        Edition edition = new Edition();
        Language language = null;
        EditionTranslate editionTranslate = null;

        try {
            editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
            languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
            editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();

            edition = editionDAO.get(5L,connection);
            language = languageDAO.get(Locale.EN.toString(),connection);
            editionTranslate = new EditionTranslate();
            editionTranslate.setGenre("Fantasy");
            editionTranslate.setTitle("Some test title");
            editionTranslate.setRef_edition(edition.getIdEdition());
            editionTranslate.setRef_lang(language.getIdlang());
            System.out.println(editionTranslateDAO.create(editionTranslate,connection));


        } catch (AppException e) {
            e.printStackTrace();
        }

    }
    private void updateTest() throws AppException {
        EditionTranslate editionTranslate = null;
        EditionTranslateDAO editionTranslateDAO = null;
        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();

        editionTranslate = editionTranslateDAO.get(1L,connection);
        editionTranslate.setAuthor("Vasya Pupkin");

        System.out.println(editionTranslateDAO.update(editionTranslate,connection));

    }
    private void getByIdTest() throws AppException {
        EditionTranslateDAO editionTranslateDAO = null;

        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();
        EditionTranslate editionTranslate = editionTranslateDAO.get(1L,connection);
        System.out.println(editionTranslate);
    }
    private void getByNameTest() throws AppException {
        EditionTranslateDAO editionTranslateDAO = null;

        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();
        EditionTranslate editionTranslate = editionTranslateDAO.get("Some test title",connection);
        System.out.println(editionTranslate);
    }
    private void deleteByTitleTest() throws AppException {

        EditionTranslateDAO editionTranslateDAO = null;
        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();
        editionTranslateDAO.delete("Some test title",connection);
    }
    private void deleteByIdTest() throws AppException {

        EditionTranslateDAO editionTranslateDAO = null;
        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();
        System.out.println(editionTranslateDAO.delete(2L,connection));
    }
    private void getAllTest() throws AppException {
        EditionTranslateDAO editionTranslateDAO = null;

        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();
        List<EditionTranslate> list = editionTranslateDAO.getAll(connection);
        System.out.println(list);
    }
}
