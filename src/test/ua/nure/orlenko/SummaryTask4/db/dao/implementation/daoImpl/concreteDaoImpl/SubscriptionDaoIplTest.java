package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Subscription;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.SubscriptionDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

/**
 *
 */
public class SubscriptionDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public SubscriptionDaoIplTest() throws DBException {
    }

    @Test
    public void mainFunctionality() throws AppException {
        //createTest();
        //updateTest();
        //getByIdTest();
        //deleteTest();
        //getAllTest();
        getUserSubscription();
    }
    private void createTest(){
        SubscriptionDAO subscriptionDAO = null;
        UserDAO userDAO = null;
        EditionDAO editionDAO = null;

        User user = null;
        Edition edition = null;
        Subscription subscription = null;

        try {
            userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
            editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
            subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();

            user = userDAO.get(2L,connection);
            edition = editionDAO.get(5L,connection);

            subscription = new Subscription();
            subscription.setPeriod(2L);
            subscription.setCost(edition.getPrice()*subscription.getPeriod());
            subscription.setSubscrdate(new Date(System.currentTimeMillis()));
            subscription.setRef_user(user.getIdUser());
            subscription.setRef_edition(edition.getIdEdition());
            System.out.println(subscriptionDAO.create(subscription,connection));


        } catch (AppException e) {
            e.printStackTrace();
        }
    }
    private void getByIdTest() throws AppException {
        SubscriptionDAO subscriptionDAO = null;

        subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();
        Subscription subscription = subscriptionDAO.get(1L,connection);
        System.out.println(subscription);
    }
    private void updateTest() throws AppException {
        Subscription subscription = null;
        SubscriptionDAO subscriptionDAO = null;
        subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();

        subscription = subscriptionDAO.get(1L,connection);
        subscription.setPeriod(3L);

        System.out.println(subscriptionDAO.update(subscription,connection));

    }
    private void deleteTest() throws AppException {

        SubscriptionDAO subscriptionDAO = null;
        subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();
        subscriptionDAO.delete(4L,connection);
    }
    private void getAllTest() throws AppException {
        SubscriptionDAO subscriptionDAO = null;

        subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();
        List<Subscription> list = subscriptionDAO.getAll(connection);
        System.out.println(list);
    }
    private void getUserSubscription() throws AppException {
        SubscriptionDAO subscriptionDAO = null;

        subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();
        List<Subscription> list = subscriptionDAO.getUserSubscription(2L,connection);
        System.out.println(list);
    }
}
