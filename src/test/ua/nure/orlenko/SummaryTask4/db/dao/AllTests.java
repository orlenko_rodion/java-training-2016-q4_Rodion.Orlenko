package ua.nure.orlenko.SummaryTask4.db.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl.AccountDaoIplTest;
import ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl.LanguageDaoIplTest;

/**
 * Test runner
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { AccessorImplTest.class, AccountDaoIplTest.class, LanguageDaoIplTest.class } )
public class AllTests {
}
