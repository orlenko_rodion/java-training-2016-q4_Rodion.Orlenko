package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Account;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.AccountDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserDAO;
import ua.nure.orlenko.SummaryTask4.enumeration.Role;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.enumeration.Gender;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

/**
 * Test class
 */
public class UserDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public UserDaoIplTest() throws DBException {
    }

    @Test
    public void mainFunctionality() throws AppException {
//        createTest();
//        getByIdTest();
        //deleteTest();
//        updateTest();
//        getAllTest();
        getByLogin();
    }
    /**
     * Generating unique 10-char id for account
     * @param base
     * @return
     */
    private String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
    private String getAccountUniqueId() {
        java.util.Date date = Calendar.getInstance().getTime();

        Long mil = date.getTime();
        String milStr = mil.toString();
        String digest = sha256(milStr);
        return digest.substring(0,10);
        //d6e9fa1540
        //e0a67d5665
        //47950e0985
    }
    private void createTest() {
        UserDAO userDao = null;
        AccountDAO accountDAO = null;
        User user = new User();
        Account account = null;

        try {
            userDao = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
            accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();

            account = new Account();
            account.setIdAccount(getAccountUniqueId());
            account.setBalance(0D);
            accountDAO.create(account,connection);

            user.setRole(Role.User.toString());
            user.setRegisterDate(new Date(System.currentTimeMillis()));
            user.setRef_account(account.getIdAccount());
            user.setLogin("login2");
            user.setPassword("password2");
            userDao.create(user,connection);


        } catch (AppException e) {
            e.printStackTrace();
        }

    }
    private void deleteTest() throws AppException {

        UserDAO userDAO = null;
        userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
        userDAO.delete(1L,connection);
    }
    private void getByIdTest() throws AppException {
        UserDAO userDAO = null;

        userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
        User user = userDAO.get(4L,connection);
        System.out.println(user);
    }
    private void getAllTest() throws AppException {
        UserDAO userDAO = null;

        userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
        List<User> list = userDAO.getAll(connection);
        System.out.println(list);
    }
    private void updateTest() throws AppException {
        User user = null;
        UserDAO userDAO = null;
        userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();

        user = userDAO.get(4L,connection);
        user.setGender(Gender.Male.toString());

        System.out.println(userDAO.update(user,connection));

    }
    private void getByLogin() throws AppException {
        UserDAO userDAO = null;

        userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
        User user = userDAO.getByLogin("login1",connection);
        System.out.println(user);
    }
}
