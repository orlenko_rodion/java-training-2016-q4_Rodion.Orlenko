package ua.nure.orlenko.SummaryTask4.db.dao.implementation.daoImpl.concreteDaoImpl;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.util.List;



/**
 * Test class
 */
public class LanguageDaoIplTest {
    Connection connection = new AccessorImpl().getConnection();

    public LanguageDaoIplTest() throws DBException {
    }

    @Test
    public void mainFunctionality() throws AppException {
        createTest();
        getByNameTest();
        getByIdTest();
        updateTest();
        getAllTest();
        deleteByName();
    }
    private void createTest() {
        LanguageDAO languageDAO = null;
        Language lang = new Language("kz","kazah");
        try {
            languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
            lang = languageDAO.create(lang,connection);
            System.out.println(lang);
        } catch (AppException e) {
            e.printStackTrace();
        }

    }
    private void updateTest() throws AppException {
        Language language = null;
        LanguageDAO languageDAO = null;

        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
        language = languageDAO.get("en",connection);
        language.setName(Locale.EN.toString());
        languageDAO.update(language,connection);

    }
    private void getByNameTest() throws AppException {
        LanguageDAO languageDAO = null;

        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
        Language lang = languageDAO.get("kz",connection);
        System.out.println(lang);

        //assertEquals(lang.getBalance(),(Double)10D);
    }
    private void getByIdTest() throws AppException {
        LanguageDAO languageDAO = null;

        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
        Language lang = languageDAO.get(1L,connection);
        System.out.println(lang);
    }
    private void deleteByName() throws AppException {

        LanguageDAO languageDAO = null;
        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
        languageDAO.delete("kz",connection);
    }
    private void getAllTest() throws AppException {
        LanguageDAO languageDAO = null;

        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
        List<Language> list = languageDAO.getAll(connection);
        System.out.println(list);
    }
}
