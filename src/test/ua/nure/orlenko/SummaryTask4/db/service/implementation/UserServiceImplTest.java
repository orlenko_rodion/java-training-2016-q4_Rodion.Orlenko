package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.enumeration.Role;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.enumeration.Gender;

import java.sql.Date;

/**
 * Test Class
 */
public class UserServiceImplTest {
    @Test
    public void mainFunctionality() throws AppException {
//        createTest();
//        getByIdTest();
//        updateTest();
        getAllTest();
//        getByNameTest();
//        banTest();
//        unBanTest();
    }
    private void getByIdTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        System.out.println(userService.getUser(3L));

    }
    private void getByNameTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        UserServiceEntity userServiceEntity = userService.getUser("Vasya", "Pupkin");

        System.out.println(userServiceEntity.getFirstName(Locale.EN));
        System.out.println(userServiceEntity.getFirstName(Locale.RU));

    }
    private void banTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();

        UserServiceEntity admin = userService.getUser(2L);
        UserServiceEntity user = userService.getUser(3L);

        userService.banUser(admin, user);
    }
    private void unBanTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();

        UserServiceEntity admin = userService.getUser(2L);
        UserServiceEntity user = userService.getUser(3L);

        userService.unBanUser(admin, user);
    }
    private void getAllTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();

        System.out.println(userService.getAll());
    }
    private void updateTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        UserServiceEntity user = userService.getUser(7L);

        user.setRole(Role.Admin);
        user.setLogin("Vasya");
        user.setPassword("password");
        user = userService.updateUser(user);
        System.out.println(user);

    }
    private void createTest() throws AppException {
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        UserServiceEntity user = new UserServiceEntity();
        user.setRegisterDate(new Date(System.currentTimeMillis()));
        user.setGender(Gender.Male);
        user.setRole(Role.User);
        user.setLogin("Petay111");
        user.setPassword("password");
        user.setName("Petya", "Petichkin", Locale.EN);
        user.setName("Петя", "Петичкин", Locale.RU);

        user = userService.createUser(user);
        System.out.println(user);

    }

}
