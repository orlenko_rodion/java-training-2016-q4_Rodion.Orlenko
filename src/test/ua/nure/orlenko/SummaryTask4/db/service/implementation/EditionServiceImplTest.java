package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;

import java.sql.Date;

/**
 * Created by Rodion-PC on 1/29/2017.
 */
public class EditionServiceImplTest {
    @Test
    public void mainFunctionality() throws AppException {
//        createTest();
//        getByIdTest();
//        getByTitleTest();
//        updateTest();
        getAllTest();
    }
    private void updateTest() throws AppException {
        EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();
        EditionServiceEntity edition = editionService.getEdition(8L);

        edition.setPublicationDate(new Date(System.currentTimeMillis()));
        edition.setTitle("Второе тестовое издание", Locale.RU);
        edition.setGenre("Фэнтези", Locale.RU);
        edition = editionService.updateEdition(edition);
        System.out.println(edition);

    }
    private void getAllTest() throws AppException {
        EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();

        System.out.println(editionService.getAll());
    }
    private void createTest() throws AppException {
        EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();
        EditionServiceEntity editionServiceEntity = new EditionServiceEntity();

        editionServiceEntity.setPrice(9999D);
        editionServiceEntity.setTitle("Fourth test edition", Locale.EN);
        editionServiceEntity.setGenre("Fantasy", Locale.EN);

        editionServiceEntity.setTitle("Четвертое тестовое издание", Locale.RU);
        editionServiceEntity.setGenre("Фэнтези", Locale.RU);

        editionServiceEntity = editionService.createEdition(editionServiceEntity);
        System.out.println(editionServiceEntity);
    }
    private void getByTitleTest() throws AppException {
        EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();
        EditionServiceEntity editionServiceEntity = editionService.getEdition("Четвертое тестовое издание");

        System.out.println(editionServiceEntity.getTitle(Locale.EN));
        System.out.println(editionServiceEntity.getTitle(Locale.RU));
    }
    public void getByIdTest() {

    }
}
