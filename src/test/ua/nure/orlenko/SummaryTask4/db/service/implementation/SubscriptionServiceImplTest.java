package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.junit.Test;
import ua.nure.orlenko.SummaryTask4.db.service.entities.SubscriptionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.db.service.SubscriptionService;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;

/**
 * Test class
 */
public class SubscriptionServiceImplTest {
    @Test
    public void mainFunctionality () throws AppException {
//        createTest();
//        getUserSubscrTest();
        getEditionSubscrTest();
    }
    private void createTest() throws AppException {
        SubscriptionService subscriptionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getSubscriptionService();

        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();

        UserServiceEntity user = userService.loginUser("Vasya", "password");
        EditionServiceEntity edition = editionService.getEdition("Second test edition");
        SubscriptionServiceEntity subscription = subscriptionService.createSubscription(user,edition,2);

        System.out.println(subscription);
    }
    private void getUserSubscrTest() throws AppException {
        SubscriptionService subscriptionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getSubscriptionService();
        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        UserServiceEntity userServiceEntity = userService.loginUser("Vasya", "password");
        System.out.println(subscriptionService.getUserSubscriptions(userServiceEntity));
    }
    private void getEditionSubscrTest() throws AppException {
        SubscriptionService subscriptionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getSubscriptionService();
        EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();
        EditionServiceEntity editionServiceEntity = editionService.getEdition(8L);
        System.out.println(subscriptionService.getEditionSubscriptions(editionServiceEntity));
    }
}
