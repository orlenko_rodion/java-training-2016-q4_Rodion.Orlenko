<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.List" %>
<%@ page import="ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<html>
<head>

    <title>Periodicals editions</title>

    <script src= "<c:url value="/styles/bootstrap/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/styles/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value="/styles/bootstrap/css/bootstrap.css"/>">

    <link rel="stylesheet" href="<c:url value="/styles/index.css"/>">
    <link rel="stylesheet" href="<c:url value="/styles/others/listEditions.css"/>">



</head>
<body>

<div id="indexContent">
    <div id="indexHeader">
        <%@ include file="/WEB-INF/jsp/header.jsp"%>
    </div>
    <div id="indexMainContent">
        <div id="indexCentralColumn">

            <div id="content">

              <%--  <c:forEach var="el" items="${list}" varStatus="rowCounter">
                    <tr>
                        <td><c:out value="${rowCounter.count}"/></td>
                        <td><c:out value="${el.getDefaultTranslate().getTitle()}"/></td>
                        <td><c:out value="${el.getDefaultTranslate().getGenre()}"/></td>
                    </tr>
                </c:forEach>--%>
                <center>
                    <span class="label label-primary"><%=bundle.getString("listEdition.allEditions")%></span>
                </center>

                <table class="table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th><%=bundle.getString("listEdition.title")%></th>
                        <th><%=bundle.getString("listEdition.genre")%></th>
                        <th><%=bundle.getString("listEdition.topic")%></th>
                        <th><%=bundle.getString("listEdition.author")%></th>
                        <th><%=bundle.getString("listEdition.publisher")%></th>
                        <th><%=bundle.getString("listEdition.publDate")%></th>
                        <th><%=bundle.getString("listEdition.price")%></th>
                        <th>Total Benefit</th>
                        <th>Total Subscribers</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <%
                            List<EditionServiceEntity> list = (List<EditionServiceEntity>) request.getAttribute("list");
                            int i = 1;
                            for (EditionServiceEntity e: list) {
                        %>

                          <tr>

                              <td> <span class="label label-default"><%=i%></span> </td>
                              <td> <span class="label label-primary"><%=e.getDefaultTranslate().getTitle()%></span> </td>
                              <td> <span class="label label-default"><%=e.getDefaultTranslate().getGenre()%></span> </td>
                              <td> <span class="label label-default"><%if (e.getDefaultTranslate().getTopic() != null){%><%=e.getDefaultTranslate().getTopic()%><%}%></span> </td>
                              <td> <span class="label label-default"><%if (e.getDefaultTranslate().getAuthor() != null){%><%=e.getDefaultTranslate().getAuthor()%><%}%></span> </td>
                              <td> <span class="label label-default"><%if (e.getDefaultTranslate().getPublisher() != null){%><%=e.getDefaultTranslate().getPublisher()%><%}%></span> </td>
                              <td> <span class="label label-default"><%if (e.getPublicationDate() != null){%><%=e.getPublicationDate()%><%}%></span> </td>
                              <td> <span class="label label-info"><%if (e.getPrice() != null){%><%=e.getPrice()%><%}%></span> </td>

                              <td> <span class="label label-default"><%if (e.getTotalSum() != null){%><%=e.getTotalSum()%><%}%></span> </td>
                              <td> <span class="label label-default"><%if (e.getUserSubscr() != null){%><%=e.getUserSubscr()%><%}%></span> </td>
                              <%--<td> <input class="input input-group-sm" type="range" min="0" max="12" step="1" name="period"> </td>--%>
                               <td>
                                   <form action="/controller" method="post">
                                   <input type="hidden" name="command" value="subscribe">

                                      <select class="form-group-sm" id="sel3" name="period">
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="5">5</option>
                                          <option value="6">6</option>
                                          <option value="7">7</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                      </select>
                                       <button class="btn btn-default" type="submit" name="title" value="<%=e.getDefaultTranslate().getTitle()%>"><%=bundle.getString("listEdition.subscribe")%></button>
                                   </form>
                               </td>

                          </tr>

                        <%i++;}%>
                    </tbody>
                </table>

            </div>

        </div>
        <div id="indexRightColumn">
            <%@ include file="/WEB-INF/jsp/rightColumn.jsp"%>
        </div>
    </div>
    <div id="indexFooter">

    </div>
</div>


</body>
</html>