<%--
  Created by IntelliJ IDEA.
  User: Rodion-PC
  Date: 1/30/2017
  Time: 10:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>

    <script src= "<c:url value="/styles/bootstrap/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/styles/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value="/styles/bootstrap/css/bootstrap.css"/>">

    <link rel="stylesheet" href="<c:url value="/styles/index.css"/>">
</head>
<%String message = (String) request.getAttribute("statusMessage");%>
<body>
<div id="indexContent">
    <div id="indexHeader">
        <%@ include file="../header.jsp"%>
    </div>
    <div id="indexMainContent">
        <div id="indexCentralColumn">
            <label class="label"></label>
            <center>
                <h2>
                    <%if (message == null) {%>
                    <label class="label label-warning" ><%=bundle.getString("error.unknown")%></label>
                    <%} else {%>
                    <label class="label label-info" ><%=message%></label>
                    <%}%>
                </h2>
            </center>

        </div>
        <div id="indexRightColumn">
            <%@ include file="/WEB-INF/jsp/rightColumn.jsp"%>
        </div>
    </div>
    <div id="indexFooter">

    </div>
</div>

</body>
</html>
