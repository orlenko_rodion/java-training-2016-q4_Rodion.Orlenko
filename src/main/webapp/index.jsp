
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<html>
<head>

    <title>Periodicals editions</title>

    <script src= "<c:url value="/styles/bootstrap/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/styles/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value="/styles/bootstrap/css/bootstrap.css"/>">

    <link rel="stylesheet" href="<c:url value="/styles/index.css"/>">



</head>
<body>

<div id="indexContent">
    <div id="indexHeader">
        <%@ include file="WEB-INF/jsp/header.jsp"%>
    </div>
    <div id="indexMainContent">
        <div id="indexCentralColumn">
            <%@ include file="WEB-INF/jsp/body.jsp"%>
        </div>
        <div id="indexRightColumn">
            <%@ include file="WEB-INF/jsp/rightColumn.jsp"%>
        </div>
    </div>
    <div id="indexFooter">

    </div>
</div>


</body>
</html>