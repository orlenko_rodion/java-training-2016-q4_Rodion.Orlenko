package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;

import java.sql.*;
import java.util.*;


/**
 *
 */
public class UserDaoImpl implements UserDAO {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(UserDaoImpl.class);

    /**
     * @param entity - insertable entity
     *               for insertion entity required minimum set of fields
     *               there are:
     *               - role
     *               - registerDate
     *               - ref_account
     * @return entity which has been inserted into database
     * @throws AppException
     */
    @Override
    public User create(User entity, Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "INSERT INTO user(Gender, RegisterDate, BirthDate,Role, Ref_Account, Login, Password) VALUES(?,?,?,?,?,?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getGender());
            statement.setDate(2,entity.getRegisterDate());
            statement.setDate(3,entity.getBirthDate());
            statement.setString(4,entity.getRole());
            statement.setString(5,entity.getRef_account());
            statement.setString(6, entity.getLogin());
            statement.setString(7, entity.getPassword());

            int rowsInserted = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsInserted > 0) {
                LOG.info("A new User was inserted successfully!");
                sql = "SELECT IdUser, Role, RegisterDate, BirthDate, Gender, Ref_Account, Login, Password FROM user WHERE Ref_Account = ? AND RegisterDate = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setString(1,entity.getRef_account());
                statement.setDate(2,entity.getRegisterDate());
                ResultSet result = statement.executeQuery();
                if (result.first()){
                    entity = new User();
                    entity.setIdUser(result.getLong(1));
                    entity.setRole(result.getString(2));
                    entity.setRegisterDate(result.getDate(3));
                    entity.setBirthDate(result.getDate(4));
                    entity.setGender(result.getString(5));
                    entity.setRef_account(result.getString(6));
                    entity.setLogin(result.getString(7));
                    entity.setPassword(result.getString(8));
                    connection.setAutoCommit(defaultAutoCommit);
                    return entity;
                }
                return null;
            } else {
                LOG.warn("A new user insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(Long id, Connection connection) throws AppException {

        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM user WHERE IdUser = ?";

            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);

            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("User was deleted successfully!");
                return true;
            } else {
                LOG.warn("User deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public User get(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdUser, Gender, RegisterDate, BirthDate, Role, Ref_Account, Login, Password FROM user WHERE IdUser = ?";

            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            User user = null;
            if (result.first()) {

                user = new User();
                user.setIdUser(result.getLong(1));
                user.setGender(result.getString(2));
                user.setRegisterDate(result.getDate(3));
                user.setBirthDate(result.getDate(4));
                user.setRole(result.getString(5));
                user.setRef_account(result.getString(6));
                user.setLogin(result.getString(7));
                user.setPassword(result.getString(8));

                LOG.trace("Selecting User entity from database wse succeeded!");
                return user;
            } else {
                LOG.warn("Selecting User entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public User update(User entity, Connection connection) throws AppException {
        LOG.debug("Calling of update method for entity -> " + entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE user SET Gender = ?, RegisterDate = ?, BirthDate = ?, Role = ?, Ref_Account = ?, Login = ?, Password = ?" +
                         "WHERE IdUser = ?";

            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getGender());
            statement.setDate(2,entity.getRegisterDate());
            statement.setDate(3,entity.getBirthDate());
            statement.setString(4,entity.getRole());
            statement.setString(5,entity.getRef_account());
            statement.setString(6,entity.getLogin());
            statement.setString(7, entity.getPassword());
            statement.setLong(8,entity.getIdUser());

            int rowsUpdated = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsUpdated > 0) {
                LOG.info("A User was updated successfully!");

                sql = "SELECT IdUser, Role, RegisterDate, BirthDate, Gender, Ref_Account, Login, Password FROM user WHERE IdUser = ?";

                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setLong(1,entity.getIdUser());
                ResultSet result = statement.executeQuery();
                if (result.first()){
                    User resUser = new User();
                    resUser.setIdUser(result.getLong(1));
                    resUser.setRole(result.getString(2));
                    resUser.setRegisterDate(result.getDate(3));
                    resUser.setBirthDate(result.getDate(4));
                    resUser.setGender(result.getString(5));
                    resUser.setRef_account(result.getString(6));
                    resUser.setLogin(result.getString(7));
                    resUser.setPassword(result.getString(8));
                    connection.setAutoCommit(defaultAutoCommit);
                    return resUser;
                }
                return null;
            } else {
                LOG.warn("A user updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<User> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdUser, Gender, RegisterDate, BirthDate, Role, Ref_Account, Login, Password FROM user";

            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<User> list = new ArrayList();
            while (result.next()) {
                User user = null;
                user = new User();
                user.setIdUser(result.getLong(1));
                user.setGender(result.getString(2));
                user.setRegisterDate(result.getDate(3));
                user.setBirthDate(result.getDate(4));
                user.setRole(result.getString(5));
                user.setRef_account(result.getString(6));
                user.setLogin(result.getString(7));
                user.setPassword(result.getString(8));

                list.add(user);
            }
            LOG.trace("Selecting User entity from database was succeeded!");
            return list;


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public User getByLogin(String login, Connection connection) throws AppException {
        LOG.debug("Calling of get method for login -> " + login);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdUser, Gender, RegisterDate, BirthDate, Role, Ref_Account, Login, Password FROM user WHERE Login = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,login);

            ResultSet result = statement.executeQuery();
            connection.setAutoCommit(defaultAutoCommit);

            User user = null;
            if (result.first()) {

                user = new User();
                user.setIdUser(result.getLong(1));
                user.setGender(result.getString(2));
                user.setRegisterDate(result.getDate(3));
                user.setBirthDate(result.getDate(4));
                user.setRole(result.getString(5));
                user.setRef_account(result.getString(6));
                user.setLogin(result.getString(7));
                user.setPassword(result.getString(8));

                LOG.trace("Selecting User entity from database wse succeeded!");
                return user;
            } else {
                LOG.warn("Selecting User entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}
