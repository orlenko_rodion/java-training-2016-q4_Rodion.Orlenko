package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

import java.sql.Date;

public class Edition implements Entity{
  private Long idEdition;
  private java.sql.Date publicationDate;
  private Double price;

  public Edition() {
  }

  public Edition(Long idEdition, Double price) {
    this.idEdition = idEdition;
    this.price = price;
  }

  public Edition(Long idEdition, Date publicationDate, Double price) {
    this.idEdition = idEdition;
    this.publicationDate = publicationDate;
    this.price = price;
  }

  public Long getIdEdition() {
    return idEdition;
  }

  public void setIdEdition(Long idEdition) {
    this.idEdition = idEdition;
  }

  public java.sql.Date getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(java.sql.Date publicationDate) {
    this.publicationDate = publicationDate;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Edition edition = (Edition) o;

    if (idEdition != null ? !idEdition.equals(edition.idEdition) : edition.idEdition != null) return false;
    if (publicationDate != null ? !publicationDate.equals(edition.publicationDate) : edition.publicationDate != null)
      return false;
    return price.equals(edition.price);
  }

  @Override
  public int hashCode() {
    int result = idEdition != null ? idEdition.hashCode() : 0;
    result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
    result = 31 * result + price.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Edition{" +
            "idEdition=" + idEdition +
            ", publicationDate=" + publicationDate +
            ", price=" + price +
            '}';
  }
}
