package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.sql.Connection;

/**
 *
 */
public interface UserDAO extends EntityDAO<User,Long>{
    User getByLogin(String login, Connection connection) throws AppException;
}
