package ua.nure.orlenko.SummaryTask4.db.service.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.Account;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.enumeration.Role;
import ua.nure.orlenko.SummaryTask4.enumeration.Gender;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.UserTranslate;

import java.sql.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class UserServiceEntity implements ServiceEntity {

    private Account account;
    private User user;
    private Map<Locale, UserTranslate> translates = new ConcurrentHashMap<>();

    public UserServiceEntity() {
        account = new Account();
        user = new User();
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    public Account getAccount() {
        return account;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Map getTranslates() {
        return translates;
    }
    public void setTranslates(Map<Locale, UserTranslate> translates) {
        this.translates = translates;
    }

    public UserTranslate getTranslate(Locale locale){
        return translates.get(locale);
    }
    public UserTranslate getDefaultTranslate() {
        for (Map.Entry<Locale, UserTranslate> pair : translates.entrySet()) {
            return pair.getValue();
        }
        return null;
    }
    public void addTranslate(Locale locale, UserTranslate translate){
        translates.put(locale, translate);
    }

    /**
     * getMethods
     */
    public Double getAccountBalance(){
        return account.getBalance();
    }

    public String getRole(){
        return user.getRole();
    }
    public Date getRegisterDate() { return user.getRegisterDate();}
    public Date getBirthDate() {return user.getBirthDate();}
    public String getGender() {return user.getGender();}
    public String getLogin() {return user.getLogin();}
    public String getPassword() {return user.getPassword();}

    public String getFirstName(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getFirstName();
        }
        return null;
    }
    public String getSecondName(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getSecondName();
        }
        return null;
    }

    /**
     * setMethods
     */
    public void setAccountBalance(Double balance){
        account.setBalance(balance);
    }

    public void setRole(Role role){
         user.setRole(role.name());
    }
    public void setRegisterDate(Date date) {
        user.setRegisterDate(date);
    }
    public void setBirhDate(Date date) {
        user.setBirthDate(date);
    }
    public void setGender(Gender gender) { user.setGender(gender.toString());}
    public void setLogin(String login) { user.setLogin(login);}
    public void setPassword(String password) { user.setPassword(password);}

    public void setName(String firsName, String secondName, Locale locale) {

        if (translates.containsKey(locale)){
            translates.get(locale).setFirstName(firsName);
            translates.get(locale).setSecondName(secondName);
        } else {
            UserTranslate userTranslate = new UserTranslate();
            userTranslate.setFirstName(firsName);
            userTranslate.setSecondName(secondName);

            translates.put(locale,userTranslate);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserServiceEntity userServiceEntity = (UserServiceEntity) o;

        if (account != null ? !account.equals(userServiceEntity.account) : userServiceEntity.account != null) return false;
        if (user != null ? !user.equals(userServiceEntity.user) : userServiceEntity.user != null) return false;
        return translates != null ? translates.equals(userServiceEntity.translates) : userServiceEntity.translates == null;
    }

    @Override
    public int hashCode() {
        int result = account != null ? account.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (translates != null ? translates.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserServiceEntity{" +
                "account=" + account +
                ", user=" + user +
                ", translates=" + translates +
                '}';
    }
}
