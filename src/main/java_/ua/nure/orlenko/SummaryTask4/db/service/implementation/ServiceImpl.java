package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Accessor;

/**
 * Created by Rodion-PC on 2/5/2017.
 */
public abstract class ServiceImpl {

    private Accessor accessor;

    public ServiceImpl(Accessor accessor) {
        this.accessor = accessor;
    }

    public Accessor getAccessor() {
        return accessor;
    }

    public void setAccessor(Accessor accessor) {
        this.accessor = accessor;
    }
}
