package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Accessor;
import ua.nure.orlenko.SummaryTask4.db.service.LanguageService;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.util.List;

/**
 * Created by Rodion-PC on 1/28/2017.
 */
public class LanguageServiceImpl extends ServiceImpl implements LanguageService {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(LanguageServiceImpl.class);

    private LanguageDAO languageDAO;

    public LanguageServiceImpl(Accessor accessor) throws DBException {
        super(accessor);
        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
    }

    public LanguageServiceImpl(DaoFactory factory, Accessor accessor) throws DBException {
        super(accessor);
        languageDAO = factory.getLanguageDao();
    }


    @Override
    public Language addLanguage(Locale locale) throws AppException {

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        Language language = new Language();
        language.setName(locale.name());
        language.setDescription(locale.getDescription());
        return languageDAO.create(language, connection);
    }

    @Override
    public Language getLanguage(Locale locale) throws AppException {
        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        return languageDAO.get(locale.name(), connection);
    }

    @Override
    public List<Language> getAll() throws AppException {
        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        return languageDAO.getAll(connection);
    }
}
