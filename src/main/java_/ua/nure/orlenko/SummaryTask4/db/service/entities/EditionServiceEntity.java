package ua.nure.orlenko.SummaryTask4.db.service.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.EditionTranslate;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;

import java.sql.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class EditionServiceEntity implements ServiceEntity{

    private Edition edition;
    private Map<Locale,EditionTranslate> translates = new ConcurrentHashMap<>();
    private Double totalSum = 0D;
    private Integer userSubscr = 0;

    public EditionServiceEntity() {
        edition = new Edition();
    }

    public EditionServiceEntity(Edition edition) {
        this.edition = edition;
    }

    public Double getTotalSum() {
        return totalSum;
    }
    public Integer getUserSubscr() {
        return userSubscr;
    }

    public void setTotalSum(Double totalSum) {
        this.totalSum = totalSum;
    }
    public void setUserSubscr(Integer userSubscr) {
        this.userSubscr = userSubscr;
    }

    public Edition getEdition() {
        return edition;
    }
    public void setEdition(Edition edition) {
        this.edition = edition;
    }

    public Map<Locale, EditionTranslate> getTranslates() {
        return translates;
    }
    public void setTranslates(Map<Locale, EditionTranslate> translates) {
        this.translates = translates;
    }

    public void addTranslate(Locale locale, EditionTranslate translate) {
        translates.put(locale, translate);
    }
    public EditionTranslate getTranslate(Locale locale) {
        return translates.get(locale);
    }
    public EditionTranslate getDefaultTranslate() {
        for (Map.Entry<Locale, EditionTranslate> pair : translates.entrySet()) {
            return pair.getValue();
        }
        return null;
    }

    /**
     * getMethods
     */
    public Date getPublicationDate() {
        return edition.getPublicationDate();
    }
    public Double getPrice() {
        return edition.getPrice();
    }

    public String getTitle(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getTitle();
        }
        return null;
    }
    public String getAuthor(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getAuthor();
        }
        return null;
    }
    public String getPublisher(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getPublisher();
        }
        return null;
    }
    public String getGenre(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getGenre();
        }
        return null;
    }
    public String getTopic(Locale locale) {
        if (translates.containsKey(locale)) {
            return translates.get(locale).getTopic();
        }
        return null;
    }

    /**
     * setMethods
     */
    public void setPublicationDate(Date date) {
        edition.setPublicationDate(date);
    }
    public void setPrice(Double price) {
        edition.setPrice(price);
    }

    public void setTitle(String title, Locale locale) {
        if (translates.containsKey(locale)){
            translates.get(locale).setTitle(title);
        } else {
            EditionTranslate editionTranslate = new EditionTranslate();
            editionTranslate.setTitle(title);

            translates.put(locale,editionTranslate);
        }

    }
    public void setAuthor(String author, Locale locale) {
        translates.get(locale).setAuthor(author);
    }
    public void setPublisher(String publisher, Locale locale) {
        translates.get(locale).setPublisher(publisher);
    }
    public void setGenre(String genre, Locale locale) {
        if (translates.containsKey(locale)){
            translates.get(locale).setGenre(genre);
        } else {
            EditionTranslate editionTranslate = new EditionTranslate();
            editionTranslate.setGenre(genre);

            translates.put(locale,editionTranslate);
        }
    }
    public void setTopic(String topic, Locale locale) {
        translates.get(locale).setTopic(topic);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EditionServiceEntity editionServiceEntity = (EditionServiceEntity) o;

        return edition.equals(editionServiceEntity.edition);
    }

    @Override
    public int hashCode() {
        return edition.hashCode();
    }

    @Override
    public String toString() {
        return "EditionServiceEntity{" +
                "edition=" + edition +
                ", translates=" + translates +
                '}';
    }
}
