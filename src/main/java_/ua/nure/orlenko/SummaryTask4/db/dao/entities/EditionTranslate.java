package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

public class EditionTranslate implements Entity{
  private Long id_editiontr;
  private String author;
  private String publisher;
  private String genre;
  private String topic;
  private String title;
  private Long ref_edition;
  private Long ref_lang;

  public Long getId_editiontr() {
    return id_editiontr;
  }

  public void setId_editiontr(Long id_editiontr) {
    this.id_editiontr = id_editiontr;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public String getGenre() {
    return genre;
  }

  public void setGenre(String genre) {
    this.genre = genre;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getRef_edition() {
    return ref_edition;
  }

  public void setRef_edition(Long ref_edition) {
    this.ref_edition = ref_edition;
  }

  public Long getRef_lang() {
    return ref_lang;
  }

  public void setRef_lang(Long ref_lang) {
    this.ref_lang = ref_lang;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    EditionTranslate that = (EditionTranslate) o;

    if (id_editiontr != null ? !id_editiontr.equals(that.id_editiontr) : that.id_editiontr != null) return false;
    if (author != null ? !author.equals(that.author) : that.author != null) return false;
    if (publisher != null ? !publisher.equals(that.publisher) : that.publisher != null) return false;
    if (!genre.equals(that.genre)) return false;
    if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;
    if (!title.equals(that.title)) return false;
    if (!ref_edition.equals(that.ref_edition)) return false;
    return ref_lang.equals(that.ref_lang);
  }

  @Override
  public int hashCode() {
    int result = id_editiontr != null ? id_editiontr.hashCode() : 0;
    result = 31 * result + (author != null ? author.hashCode() : 0);
    result = 31 * result + (publisher != null ? publisher.hashCode() : 0);
    result = 31 * result + genre.hashCode();
    result = 31 * result + (topic != null ? topic.hashCode() : 0);
    result = 31 * result + title.hashCode();
    result = 31 * result + ref_edition.hashCode();
    result = 31 * result + ref_lang.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "EditionTranslate{" +
            "id=" + id_editiontr +
            ", author='" + author + '\'' +
            ", publisher='" + publisher + '\'' +
            ", genre='" + genre + '\'' +
            ", topic='" + topic + '\'' +
            ", title='" + title + '\'' +
            ", ref_edition=" + ref_edition +
            ", ref_lang=" + ref_lang +
            '}';
  }
}
