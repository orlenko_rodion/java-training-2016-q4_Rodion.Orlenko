package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.exception.Messages;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
/**
 * class-implementor of LanguageDAO interface
 * provides an access to DB information (mostly throw the CRUD methods)
 */
public class EditionDaoImpl implements EditionDAO {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(EditionDaoImpl.class);

    /**
     *
     * @param entity - insertable entity
     *               for insertion entity required minimum set of fields
     *               there are:
     *               - price
     *
     * @return entity which has been inserted into database
     * @throws AppException
     */
    @Override
    public Edition create(Edition entity, Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "INSERT INTO edition(PublicationDate, Price) VALUES(?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setDate(1, entity.getPublicationDate());
            statement.setDouble(2, entity.getPrice());

            connection.setAutoCommit(defaultAutoCommit);


            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {

                LOG.info("A new Edition was inserted successfully!");
                sql = "SELECT IdEdition, PublicationDate, Price\n" +
                        "FROM edition " +
                        "WHERE IdEdition = (SELECT MAX(IdEdition) FROM edition)";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);

                ResultSet result = statement.executeQuery();
                if (result.next()){
                    Edition edition = new Edition();
                    edition.setIdEdition(result.getLong(1));
                    edition.setPublicationDate(result.getDate(2));
                    edition.setPrice(result.getDouble(3));
                    connection.setAutoCommit(defaultAutoCommit);
                    return edition;
                }
                return null;
            } else {
                LOG.warn("A new Edition insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }
        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM edition WHERE IdEdition = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);

            connection.setAutoCommit(defaultAutoCommit);

            int rowsProceeded = statement.executeUpdate();
            if (rowsProceeded > 0) {
                LOG.info("Edition was deleted successfully!");
                return true;
            } else {
                LOG.warn("Edition deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Edition get(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdEdition, PublicationDate, Price FROM edition WHERE IdEdition = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            Edition edition = null;
            if (result.next()) {

                edition = new Edition();
                edition.setIdEdition(result.getLong(1));
                edition.setPublicationDate(result.getDate(2));
                edition.setPrice(result.getDouble(3));

                LOG.trace("Selecting User entity from database wse succeeded!");
                return edition;
            } else {
                LOG.warn("Selecting User entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Edition update(Edition entity, Connection connection) throws AppException {
        LOG.debug("Calling of update method for entity -> " + entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE edition SET PublicationDate = ?, Price = ?" +
                        " WHERE IdEdition = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setDate(1,entity.getPublicationDate());
            statement.setDouble(2,entity.getPrice());
            statement.setLong(3,entity.getIdEdition());

            connection.setAutoCommit(defaultAutoCommit);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                LOG.info("A User was updated successfully!");
                sql = "SELECT IdEdition, PublicationDate, Price FROM edition WHERE IdEdition = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setLong(1,entity.getIdEdition());
                ResultSet result = statement.executeQuery();

                Edition edition = null;
                if (result.first()) {
                    edition = new Edition();
                    edition.setIdEdition(result.getLong(1));
                    edition.setPublicationDate(result.getDate(2));
                    edition.setPrice(result.getDouble(3));

                    LOG.trace("Selecting User entity from database wse succeeded! ");
                    return edition;
                }
                return null;

            } else {
                LOG.warn("A user updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<Edition> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdEdition, PublicationDate, Price FROM edition";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<Edition> list = new ArrayList();
            while (result.next()) {
                Edition edition = null;
                edition = new Edition();
                edition.setIdEdition(result.getLong(1));
                edition.setPublicationDate(result.getDate(2));
                edition.setPrice(result.getDouble(3));

                list.add(edition);
            }
            LOG.trace("Selecting User entity from database was succeeded!");
            return list;

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}
