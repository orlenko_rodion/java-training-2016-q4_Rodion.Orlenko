package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.sql.Connection;
import java.util.List;


public interface EntityDAO<E extends Entity,K> {
    E create(E entity, Connection connection) throws AppException;
    boolean delete(K id, Connection connection) throws AppException;
    E get(K id, Connection connection) throws AppException;
    E update(E entity, Connection connection) throws AppException;
    List<E> getAll(Connection connection) throws AppException;
}
