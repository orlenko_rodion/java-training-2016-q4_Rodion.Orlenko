package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.Subscription;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.sql.Connection;
import java.util.List;

/**
 *
 */
public interface SubscriptionDAO extends EntityDAO<Subscription,Long> {
    List<Subscription> getUserSubscription(Long userId, Connection connection) throws AppException;
    List<Subscription> getEditionSubscription(Long edition, Connection connection) throws AppException;
}
