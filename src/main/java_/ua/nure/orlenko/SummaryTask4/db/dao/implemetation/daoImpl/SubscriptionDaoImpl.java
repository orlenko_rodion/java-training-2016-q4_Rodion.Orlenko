package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Subscription;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.SubscriptionDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * class-implementor of SubscriptionDAO interface
 * provides an access to DB information (mostly throw the CRUD methods)
 */
public class SubscriptionDaoImpl implements SubscriptionDAO {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(SubscriptionDaoImpl.class);

    /**
     *
     * @param entity insertable entity
     *               for insertion, entity required all fields except Id
     * @return inserted entity
     * @throws AppException
     */
    @Override
    public Subscription create(Subscription entity,Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "INSERT INTO subscription(SubscrDate, Period, Cost, Ref_User, Ref_Edition) VALUES(?,?,?,?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setDate(1,entity.getSubscrdate());
            statement.setLong(2,entity.getPeriod());
            statement.setDouble(3,entity.getCost());
            statement.setLong(4,entity.getRef_user());
            statement.setLong(5,entity.getRef_edition());

            int rowsInserted = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsInserted > 0) {
                LOG.info("A new Subscription was inserted successfully!");
                sql = "SELECT IdSubscr, SubscrDate, Period, Cost, Ref_User, Ref_Edition\n" +
                        "FROM subscription " +
                        "WHERE IdSubscr = (Select max(IdSubscr) FROM subscription)";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                ResultSet result = statement.executeQuery();

                if (result.first()){
                    Subscription subscription = new Subscription();
                    subscription.setIdsubscr(result.getLong(1));
                    subscription.setSubscrdate(result.getDate(2));
                    subscription.setPeriod(result.getLong(3));
                    subscription.setCost(result.getDouble(4));
                    subscription.setRef_user(result.getLong(5));
                    subscription.setRef_user(result.getLong(6));
                    connection.setAutoCommit(defaultAutoCommit);
                    return subscription;
                }
                return null;

            } else {
                LOG.warn("A new UserTranslate insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }
        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM subscription WHERE IdSubscr = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);

            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("UserTranslate was deleted successfully!");
                return true;
            } else {
                LOG.warn("UserTranslate deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Subscription get(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT  IdSubscr, SubscrDate, Period, Cost, Ref_User, Ref_Edition FROM subscription WHERE IdSubscr = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            Subscription subscription = null;
            if (result.first()) {

                subscription = new Subscription();
                subscription.setIdsubscr(result.getLong(1));
                subscription.setSubscrdate(result.getDate(2));
                subscription.setPeriod(result.getLong(3));
                subscription.setCost(result.getDouble(4));
                subscription.setRef_user(result.getLong(5));
                subscription.setRef_edition(result.getLong(6));

                LOG.trace("Selecting UserTranslate entity from database wse succeeded!");
                return subscription;
            } else {
                LOG.warn("Selecting UserTranslate entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Subscription update(Subscription entity, Connection connection) throws AppException {
        LOG.debug("Calling of update method for entity -> " + entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE subscription SET  SubscrDate = ?, Period = ?, Cost = ?, Ref_User = ?, Ref_Edition = ? " +
                         "WHERE IdSubscr = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setDate(1,entity.getSubscrdate());
            statement.setLong(2,entity.getPeriod());
            statement.setDouble(3,entity.getCost());
            statement.setLong(4,entity.getRef_user());
            statement.setLong(5,entity.getRef_edition());
            statement.setLong(6,entity.getIdsubscr());


            int rowsUpdated = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsUpdated > 0) {
                LOG.info("A UserTranslate was updated successfully!");
                sql = "SELECT IdSubscr, SubscrDate, Period, Cost, Ref_User, Ref_Edition\n" +
                        "FROM subscription\n" +
                        "WHERE IdSubscr = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setLong(1,entity.getIdsubscr());

                ResultSet result = statement.executeQuery();
                if (result.first()){
                    Subscription subscription = new Subscription();
                    subscription.setIdsubscr(result.getLong(1));
                    subscription.setSubscrdate(result.getDate(2));
                    subscription.setPeriod(result.getLong(3));
                    subscription.setCost(result.getDouble(4));
                    subscription.setRef_user(result.getLong(5));
                    subscription.setRef_user(result.getLong(6));
                    connection.setAutoCommit(defaultAutoCommit);
                    return subscription;
                }
                return null;
            } else {
                LOG.warn("A userTranslate updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<Subscription> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdSubscr, SubscrDate, Period, Cost, Ref_User, Ref_Edition\n" +
                         "FROM subscription";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<Subscription> list = new ArrayList();
            while (result.next()) {
                Subscription subscription = new Subscription();
                subscription.setIdsubscr(result.getLong(1));
                subscription.setSubscrdate(result.getDate(2));
                subscription.setPeriod(result.getLong(3));
                subscription.setCost(result.getDouble(4));
                subscription.setRef_user(result.getLong(5));
                subscription.setRef_edition(result.getLong(6));

                list.add(subscription);
            }
            LOG.trace("Selecting Subscription entity from database was succeeded!");
            return list;


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<Subscription> getUserSubscription(Long userId, Connection connection) throws AppException {
        LOG.debug("Calling of getUserSubscription method for userId " + userId);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdSubscr, SubscrDate, Period, Cost, Ref_User, Ref_Edition\n" +
                    "FROM subscription\n" +
                    "WHERE Ref_User = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1, userId);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            List<Subscription> list = new ArrayList();
            while (result.next()) {
                Subscription subscription = new Subscription();
                subscription.setIdsubscr(result.getLong(1));
                subscription.setSubscrdate(result.getDate(2));
                subscription.setPeriod(result.getLong(3));
                subscription.setCost(result.getDouble(4));
                subscription.setRef_user(result.getLong(5));
                subscription.setRef_edition(result.getLong(6));

                list.add(subscription);
            }
            LOG.trace("Selecting Subscription entity from database was succeeded!");
            return list;


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
    @Override
    public List<Subscription> getEditionSubscription(Long editionId, Connection connection) throws AppException {
        LOG.debug("Calling of getEditionSubscription method for userId " + editionId);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdSubscr, SubscrDate, Period, Cost, Ref_User, Ref_Edition\n" +
                    "FROM subscription\n" +
                    "WHERE Ref_Edition = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1, editionId);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            List<Subscription> list = new ArrayList();
            while (result.next()) {
                Subscription subscription = new Subscription();
                subscription.setIdsubscr(result.getLong(1));
                subscription.setSubscrdate(result.getDate(2));
                subscription.setPeriod(result.getLong(3));
                subscription.setCost(result.getDouble(4));
                subscription.setRef_user(result.getLong(5));
                subscription.setRef_edition(result.getLong(6));

                list.add(subscription);
            }
            LOG.trace("Selecting Subscription entity from database was succeeded!");
            return list;


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}
