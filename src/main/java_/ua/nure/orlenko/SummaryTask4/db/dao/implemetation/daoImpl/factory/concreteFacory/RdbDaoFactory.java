package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.concreteFacory;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.*;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.*;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

/**
 * Created by Rodion-PC on 1/23/2017.
 */
public class RdbDaoFactory extends DaoFactory {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(RdbDaoFactory.class);

    public AccountDAO getAccountDAO() throws DBException {
//        AccountDaoImp accountDao = null;
//        LOG.info("Instantiating AccountDAO object ...");
//        try {
//            accountDao = new AccountDaoImp(new AccessorImpl());
//            LOG.info("AccountDAO was obtained => " + accountDao);
//        } catch (DBException e) {
//            LOG.error(Messages.ERR_CANNOT_OBTAIN_DAO,e);
//            throw new DBException(Messages.ERR_CANNOT_OBTAIN_DAO,e);
//        }
//        return  accountDao;
        return new AccountDaoImp();
    }
    public LanguageDAO getLanguageDao() throws DBException {
        return new LanguageDaoImpl();
    }

    @Override
    public UserTranslateDAO getUserTranslateDao() throws DBException {
        return  new UserTranslateDaoImpl();
    }

    @Override
    public UserDAO getUserDAO() throws DBException {
        return  new UserDaoImpl();
    }

    @Override
    public EditionDAO getEditionDAO() throws DBException {
        return  new EditionDaoImpl();
    }

    @Override
    public EditionTranslateDAO getEditionTranslateDAO() throws DBException {
      return new EditionTranslateDaoImpl();
    }

    @Override
    public SubscriptionDAO getSubscriptionDAO() throws DBException {
        return  new SubscriptionDaoImpl();
    }
}
