package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.sql.Connection;


/**
 *
 */
public interface LanguageDAO extends EntityDAO<Language,Long> {

    Language get(String name, Connection connection) throws AppException;
    boolean delete(String name, Connection connection) throws AppException;

}
