package ua.nure.orlenko.SummaryTask4.db.service;

import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.util.List;

/**
 *
 */
public interface LanguageService {
    Language addLanguage(Locale locale) throws AppException;
    Language getLanguage(Locale locale) throws AppException;
    List<Language> getAll() throws AppException;
}
