package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Account;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.AccountDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * class-implementor of AccountDAO interface
 * provides an access to DB information (mostly throw the CRUD methods)
 */
public class AccountDaoImp implements AccountDAO {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(AccountDaoImp.class);

    public Account create(Account entity, Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;
        Account resAccount = null;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "INSERT INTO Account(IdAccount, Balance) VALUES(?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getIdAccount());
            statement.setDouble(2,entity.getBalance());

            connection.setAutoCommit(defaultAutoCommit);

            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                LOG.info("A new account was inserted successfully!");
                return entity;
            } else {
                LOG.warn("A new account insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    public boolean delete(String id, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM Account WHERE IdAccount = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,id);

            connection.setAutoCommit(defaultAutoCommit);

            int rowsProceeded = statement.executeUpdate();
            if (rowsProceeded > 0) {
                LOG.info("Account was deleted successfully!");
                return true;
            } else {
                LOG.warn("Account deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    public Account get(String id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdAccount, Balance FROM Account WHERE IdAccount = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            Account account = null;
            if (result.next()) {

                account = new Account();
                account.setIdAccount(result.getString(1));
                account.setBalance(result.getDouble(2));

                LOG.trace("Selecting account entity from database wse succeeded!");
                return account;
            } else {
                LOG.warn("Selecting account entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }

    }

    public Account update(Account entity, Connection connection) throws AppException {

        LOG.debug("Calling of update method for entity -> "+ entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE account SET Balance = ? WHERE IdAccount = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setDouble(1,entity.getBalance());
            statement.setString(2,entity.getIdAccount());

            connection.setAutoCommit(defaultAutoCommit);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                LOG.info("Account was updated successfully!");
                return entity;
            } else {
                LOG.warn("Account updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    public List<Account> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdAccount, Balance FROM Account";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<Account> list = new ArrayList();
            while (result.next()) {
                Account account = null;
                account = new Account();
                account.setIdAccount(result.getString(1));
                account.setBalance(result.getDouble(2));

                list.add(account);
            }
            LOG.trace("Selecting account entity from database wse succeeded!");
            return list;

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}
