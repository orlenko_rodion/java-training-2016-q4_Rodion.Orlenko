package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.EditionTranslate;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.sql.Connection;
import java.util.List;

/**
 *
 */
public interface EditionTranslateDAO extends EntityDAO<EditionTranslate, Long> {
    EditionTranslate get(String title, Connection connection) throws AppException;
    List<EditionTranslate> getAllForEdition(Long idEdition, Connection connection)throws AppException;
    boolean delete(String title, Connection connection) throws AppException;
}
