package ua.nure.orlenko.SummaryTask4.db.service.implementation.factory;

import ua.nure.orlenko.SummaryTask4.db.service.LanguageService;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.db.service.SubscriptionService;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.concreteFactory.DefaultServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

/**
 * Created by Rodion-PC on 1/27/2017.
 */
public abstract class ServiceFactory {
    public static final int DEFAULT_SERVICE = 1;

    public abstract EditionService getEditionService() throws DBException;
    public abstract UserService getUserService() throws DBException;
    public abstract SubscriptionService getSubscriptionService() throws DBException;
    public abstract LanguageService getLanguageService() throws DBException;

    public static ServiceFactory getFactory(int whichFactory) {

        switch (whichFactory) {
            case DEFAULT_SERVICE:
                return new DefaultServiceFactory();

            default           :
                return null;
        }
    }
}
