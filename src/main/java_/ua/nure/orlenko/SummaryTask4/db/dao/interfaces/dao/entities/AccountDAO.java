package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.Account;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;

/**
 *
 */
public interface AccountDAO extends EntityDAO<Account,String> {

}
