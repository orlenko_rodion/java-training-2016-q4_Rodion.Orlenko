package ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.concreteFactory;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource.AccessorImpl;
import ua.nure.orlenko.SummaryTask4.db.service.LanguageService;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.EditionServiceImpl;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.LanguageServiceImpl;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.db.service.SubscriptionService;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.SubscriptionServiceImpl;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.UserServiceImpl;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

/**
 * Created by Rodion-PC on 1/27/2017.
 */
public class DefaultServiceFactory extends ServiceFactory {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(DefaultServiceFactory.class);
    @Override
    public EditionService getEditionService() throws DBException {
        LOG.trace("Instantiating EditionService object");
        return new EditionServiceImpl(new AccessorImpl());
    }

    @Override
    public UserService getUserService() throws DBException {
        LOG.trace("Instantiating UserService object");
        return new UserServiceImpl(new AccessorImpl());
    }

    @Override
    public SubscriptionService getSubscriptionService() throws DBException {
        LOG.trace("Instantiating UserService object");
        return new SubscriptionServiceImpl(new AccessorImpl());
    }

    @Override
    public LanguageService getLanguageService() throws DBException {
        return new LanguageServiceImpl(new AccessorImpl());
    }
}
