package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory;

import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.concreteFacory.RdbDaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.*;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

/**
 *  Factory of data access objects
 */
public abstract class DaoFactory {
    /**
     * Indicate relational database
     */
    public static final int RDB = 1;

    public abstract AccountDAO getAccountDAO() throws DBException;
    public abstract LanguageDAO getLanguageDao() throws DBException;
    public abstract UserTranslateDAO getUserTranslateDao() throws DBException;
    public abstract UserDAO getUserDAO() throws DBException;
    public abstract EditionDAO getEditionDAO() throws DBException;
    public abstract EditionTranslateDAO getEditionTranslateDAO() throws DBException;
    public abstract SubscriptionDAO getSubscriptionDAO() throws DBException;


    public static DaoFactory getFactory(int whichFactory) {

        switch (whichFactory) {
            case RDB:
                return new RdbDaoFactory();

            default           :
                return null;
        }
    }
}
