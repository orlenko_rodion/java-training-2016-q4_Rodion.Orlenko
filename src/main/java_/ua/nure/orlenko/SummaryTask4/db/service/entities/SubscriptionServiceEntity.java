package ua.nure.orlenko.SummaryTask4.db.service.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.entities.Subscription;

/**
 *
 */
public class SubscriptionServiceEntity {
    Subscription subscription;
    UserServiceEntity user;
    EditionServiceEntity edition;

    public SubscriptionServiceEntity() {
    }

    public SubscriptionServiceEntity(Subscription subscription, UserServiceEntity user, EditionServiceEntity edition) {
        this.subscription = subscription;
        this.user = user;
        this.edition = edition;
    }

    public Subscription getSubscription() {
        return subscription;
    }
    public UserServiceEntity getUser() {
        return user;
    }
    public EditionServiceEntity getEdition() {
        return edition;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
    public void setUser(UserServiceEntity user) {
        this.user = user;
    }
    public void setEdition(EditionServiceEntity edition) {
        this.edition = edition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscriptionServiceEntity that = (SubscriptionServiceEntity) o;

        if (subscription != null ? !subscription.equals(that.subscription) : that.subscription != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return edition != null ? edition.equals(that.edition) : that.edition == null;
    }

    @Override
    public int hashCode() {
        int result = subscription != null ? subscription.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (edition != null ? edition.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SubscriptionServiceEntity{" +
                "subscription=" + subscription +
                ", user=" + user +
                ", edition=" + edition +
                '}';
    }
}
