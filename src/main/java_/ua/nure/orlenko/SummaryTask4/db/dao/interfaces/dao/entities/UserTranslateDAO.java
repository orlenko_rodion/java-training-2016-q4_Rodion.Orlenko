package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.UserTranslate;

import java.sql.Connection;
import java.util.List;

/**
 *
 */
public interface UserTranslateDAO extends EntityDAO<UserTranslate,Long> {
    UserTranslate get(String firstName, String secondName, Connection connection) throws AppException;
    List<UserTranslate> getAllForUser(Long idUser, Connection connection) throws AppException;
    boolean delete(String firstName, String secondName, Connection connection) throws AppException;
}
