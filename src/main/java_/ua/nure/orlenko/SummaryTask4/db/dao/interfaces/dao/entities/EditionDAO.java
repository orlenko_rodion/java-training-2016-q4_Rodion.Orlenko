package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.EntityDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;

/**
 *
 */
public interface EditionDAO extends EntityDAO<Edition,Long> {
}
