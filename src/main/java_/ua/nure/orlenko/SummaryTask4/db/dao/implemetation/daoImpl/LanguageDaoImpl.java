package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * class-implementor of LanguageDAO interface
 * provides an access to DB information (mostly throw the CRUD methods)
 */
public class LanguageDaoImpl implements LanguageDAO {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(LanguageDaoImpl.class);

    @Override
    public Language get(String name, Connection connection) throws AppException {
        LOG.debug("Calling of get method for name -> " + name);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdLang, Name, Description FROM language WHERE Name = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,name);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            Language language = null;
            if (result.first()) {

                language = new Language();
                language.setIdlang(result.getLong(1));
                language.setName(result.getString(2));
                language.setDescription(result.getString(3));

                LOG.trace("Selecting language entity from database wse succeeded!");
                return language;
            } else {
                LOG.warn("Selecting language entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Language get(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdLang, Name, Description FROM language WHERE IdLang = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            Language language = null;
            if (result.first()) {

                language = new Language();
                language.setIdlang(result.getLong(1));
                language.setName(result.getString(2));
                language.setDescription(result.getString(3));

                LOG.trace("Selecting language entity from database wse succeeded!");
                return language;
            } else {
                LOG.warn("Selecting language entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(String name, Connection connection) throws AppException {

        LOG.debug("Calling of delete method for name -> " + name);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "DELETE FROM language WHERE Name = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,name);

            Language language = null;
            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("Language was deleted successfully!");
                return true;
            } else {
                LOG.warn("Language deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_DELETE_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "DELETE FROM language WHERE IdLang = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);

            Language language = null;
            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("Language was deleted successfully!");
                return true;
            } else {
                LOG.warn("Language deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_DELETE_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Language create(Language entity, Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;
        Language resLanguage = null;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "INSERT INTO language(Name, Description) VALUES(?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getName());
            statement.setString(2,entity.getDescription());

            int rowsInserted = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsInserted > 0) {
                LOG.info("A new language was inserted successfully!");
                sql = "SELECT IdLang FROM language WHERE Name = ? AND Description = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setString(1,entity.getName());
                statement.setString(2,entity.getDescription());
                ResultSet result = statement.executeQuery();

                if (result.next()){
                    entity.setIdlang(result.getLong(1));
                    return entity;
                }
                return null;
            } else {
                LOG.warn("A new language insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }
        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public Language update(Language entity, Connection connection) throws AppException {

        LOG.debug("Calling of update method for entity -> " + entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE language SET Name = ?, Description = ? WHERE IdLang = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getName());
            statement.setString(2,entity.getDescription());
            statement.setLong(3,entity.getIdlang());

            int rowsUpdated = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsUpdated > 0) {
                LOG.info("Language was updated successfully!");
                return entity;
            } else {
                LOG.warn("Language updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<Language> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdLang, Name, Description Balance FROM Language";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<Language> list = new ArrayList();
            while (result.next()) {
                Language language = null;
                language = new Language();
                language.setIdlang(result.getLong(1));
                language.setName(result.getString(2));
                language.setDescription(result.getString(3));

                list.add(language);
            }
            LOG.trace("Selecting language entity from database was succeeded!");
            return list;

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}