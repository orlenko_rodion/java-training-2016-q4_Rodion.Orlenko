package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

/**
 * POJO class
 * reflect certain DB dao
 */
public class UserTranslate implements Entity{
  private Long idUser;

  private String firstName;
  private String secondName;

  private Long ref_user;
  private Long ref_lang;

  public UserTranslate() {
  }

  public UserTranslate(String firstName, String secondName, Long ref_user, Long ref_lang) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.ref_user = ref_user;
    this.ref_lang = ref_lang;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public Long getRef_user() {
    return ref_user;
  }

  public void setRef_user(Long ref_user) {
    this.ref_user = ref_user;
  }

  public Long getRef_lang() {
    return ref_lang;
  }

  public void setRef_lang(Long ref_lang) {
    this.ref_lang = ref_lang;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    UserTranslate that = (UserTranslate) o;

    if (idUser != null ? !idUser.equals(that.idUser) : that.idUser != null) return false;
    if (!firstName.equals(that.firstName)) return false;
    if (!secondName.equals(that.secondName)) return false;
    if (!ref_user.equals(that.ref_user)) return false;
    return ref_lang.equals(that.ref_lang);
  }

  @Override
  public int hashCode() {
    int result = idUser != null ? idUser.hashCode() : 0;
    result = 31 * result + firstName.hashCode();
    result = 31 * result + secondName.hashCode();
    result = 31 * result + ref_user.hashCode();
    result = 31 * result + ref_lang.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "UserTranslate{" +
            "idUser=" + idUser +
            ", firstName='" + firstName + '\'' +
            ", secondName='" + secondName + '\'' +
            ", ref_user=" + ref_user +
            ", ref_lang=" + ref_lang +
            '}';
  }
}
