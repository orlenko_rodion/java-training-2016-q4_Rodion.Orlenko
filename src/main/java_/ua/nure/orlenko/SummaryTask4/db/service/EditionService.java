package ua.nure.orlenko.SummaryTask4.db.service;

import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.util.List;

/**
 *
 */
public interface EditionService {
    EditionServiceEntity createEdition(EditionServiceEntity editionEntry) throws AppException;

    EditionServiceEntity updateEdition(EditionServiceEntity editionEntry) throws AppException;

    Boolean deleteEdition(EditionServiceEntity edition) throws AppException;

    EditionServiceEntity getEdition(String title) throws AppException;
    EditionServiceEntity getEdition(Long id) throws AppException;

    List<EditionServiceEntity> getAll() throws AppException;

    List<EditionServiceEntity> SortByPrice(List<EditionServiceEntity> list);
    List<EditionServiceEntity> SortByTitle(List<EditionServiceEntity> list);
    List<EditionServiceEntity> SortByGenre(List<EditionServiceEntity> list);
}
