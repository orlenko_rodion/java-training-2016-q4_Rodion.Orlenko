package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Account;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.User;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Accessor;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.AccountDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserDAO;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.enumeration.Role;
import ua.nure.orlenko.SummaryTask4.exception.DBException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.UserTranslate;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserTranslateDAO;
import ua.nure.orlenko.SummaryTask4.enumeration.Gender;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.*;

/**
 * business logic
 * realization of UserService
 */
public class UserServiceImpl extends ServiceImpl implements UserService {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    private UserDAO userDAO;
    private AccountDAO accountDAO;
    private UserTranslateDAO userTranslateDAO;
    private LanguageDAO languageDAO;


    public UserServiceImpl(Accessor accessor) throws DBException {
        super(accessor);
        userDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserDAO();
        accountDAO = DaoFactory.getFactory(DaoFactory.RDB).getAccountDAO();
        userTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getUserTranslateDao();
        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
    }

    public UserServiceImpl(DaoFactory factory, Accessor accessor) throws DBException {
        super(accessor);
        LOG.info("Setting new DaoFactory!");

        userDAO = factory.getUserDAO();
        accountDAO = factory.getAccountDAO();
        userTranslateDAO = factory.getUserTranslateDao();
        languageDAO = factory.getLanguageDao();
    }
    /**
     * Generating unique 10-char id for account
     * @param base
     * @return
     */
    private String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
    private String getAccountUniqueId() {
        java.util.Date date = Calendar.getInstance().getTime();

        Long mil = date.getTime();
        String milStr = mil.toString();
        String digest = sha256(milStr);
        return digest.substring(0,10);
        //d6e9fa1540
        //e0a67d5665
        //47950e0985
    }

    @Override
    public UserServiceEntity getUser(String firstName, String secondName) throws AppException {
        LOG.debug("Calling of get method for name -> " + firstName + " " +
                                                         secondName );
        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        User user = null;
        Account account = null;
        List<UserTranslate> translateList = null;

        try {
            connection.setAutoCommit(false);

            UserTranslate userTranslate = null;
            userTranslate = userTranslateDAO.get(firstName,secondName, connection);

            user = userDAO.get(userTranslate.getRef_user(), connection);
            account = accountDAO.get(user.getRef_account(), connection);
            translateList = userTranslateDAO.getAllForUser(user.getIdUser(), connection);

            LOG.trace("Data validation");
            if (user == null
                    || account ==null
                    || translateList == null
                    || translateList.size() < 1) {
                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

            UserServiceEntity resUser = new UserServiceEntity();
            resUser.setAccount(account);
            resUser.setUser(user);
            for (UserTranslate translate:
                    translateList) {

                Language language = languageDAO.get(translate.getRef_lang(), connection);
                Locale locale = Locale.valueOf(language.getName());
                resUser.addTranslate(locale, translate);
            }
            connection.commit();
            return resUser;

        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    @Override
    public UserServiceEntity loginUser(String login, String password) throws AppException {
        LOG.debug("Calling of login method for login/password" + login + " " + password);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        try {
            connection.setAutoCommit(false);
            LOG.trace("Data validation");
            if (login == null
                    || login.length() < 2
                    || password == null
                    || password.length() < 2) {
                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

            LOG.trace("Logic proceeding...");
            User user = userDAO.getByLogin(login,connection);

            if (!Objects.equals(user.getPassword(), password)) {
                LOG.warn(Messages.ERR_LOGIN_FAILED);
                throw new AppException(Messages.ERR_LOGIN_FAILED);
            }

            connection.commit();
            return getUser(user.getIdUser());

        } catch (SQLException e) {
           throw new AppException(e.getMessage(), e);
        }
    }

    @Override
    public UserServiceEntity getUser(Long id) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        User user = null;
        Account account = null;
        List<UserTranslate> translateList = null;

        try {
            connection.setAutoCommit(false);

            user = userDAO.get(id,connection);
            account = accountDAO.get(user.getRef_account(), connection);
            translateList = userTranslateDAO.getAllForUser(user.getIdUser(), connection);

            LOG.trace("Data validation");
            if (user == null
                    || account ==null
                    || translateList == null
                    || translateList.size() < 1) {
                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

            UserServiceEntity resUser = new UserServiceEntity();
            resUser.setAccount(account);
            resUser.setUser(user);
            for (UserTranslate translate:
                    translateList) {

                Language language = languageDAO.get(translate.getRef_lang(), connection);
                Locale locale = Locale.valueOf(language.getName());
                resUser.addTranslate(locale, translate);
            }
            connection.commit();
            return resUser;
        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    @Override
    public List<UserServiceEntity> getAll() throws AppException {
        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        List<UserServiceEntity> result = new LinkedList<>();

        List<User> userList = userDAO.getAll(connection);

        for (User user: userList) {
            result.add(getUser(user.getIdUser()));
        }

        return result;
    }

    /**
     * Baning of user by admin
     * @param blocker - object of UsesS entity
     *                object should have minimum set of fields
     *                there are:
     *                - id or name
     *
     *                blocker selecting performs firstly by id
     *                if id is null selecting performs by name
     * @param blockaded - object of UsesS entity
     *                  object should have minimum set of fields
     *                there are:
     *                - id or name
     *
     *                blocker selecting performs firstly by id
     *                if id is null selecting performs by name
     * @return true - if operation succeeded
     *         false - if operation failed
     *         null - if were thrown unexpected exceptions
     * @throws AppException
     */
    @Override
    public Boolean banUser(UserServiceEntity blocker, UserServiceEntity blockaded) throws AppException {
        LOG.debug("Calling BanUser method of " + blocker + " -> " + blockaded);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        User blockerUser = blocker.getUser();
        UserTranslate blockerTranslate = blocker.getDefaultTranslate();

        User blockadedUser = blockaded.getUser();
        UserTranslate blockadedTranslate = blockaded.getDefaultTranslate();

        try {
            connection.setAutoCommit(false);
            LOG.trace("Data validation!");

            if ((blockerUser.getIdUser() == null
                    || blockerUser.getIdUser() < 0)
                && (blockerTranslate == null
                    || blockerTranslate.getFirstName() == null
                    || blockerTranslate.getFirstName().length() < 2
                    || blockerTranslate.getSecondName() == null
                    || blockerTranslate.getSecondName().length() < 2)) {

                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_PERFORM_OPERATION);
            }
            if ((blockadedUser.getIdUser() == null
                    || blockadedUser.getIdUser() < 0)
                    && (blockadedUser == null
                    || blockadedTranslate.getFirstName() == null
                    || blockadedTranslate.getFirstName().length() < 2
                    || blockadedTranslate.getSecondName() == null
                    || blockadedTranslate.getSecondName().length() < 2)) {

                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_PERFORM_OPERATION);
            }


            LOG.trace("Logic proceeding...");

            if (blockerUser.getIdUser() == null
                    || blockerUser.getIdUser() < 0) {
                blocker = getUser(blockerTranslate.getFirstName(),blockerTranslate.getSecondName());
            } else {
                blocker = getUser(blockerUser.getIdUser());
            }


            if (blockadedUser.getIdUser() == null
                    || blockadedUser.getIdUser() < 0) {
                blockaded = getUser(blockadedTranslate.getFirstName(),blockadedTranslate.getSecondName());
            } else {
                blockaded = getUser(blockadedUser.getIdUser());
            }



            if (Objects.equals(blocker.getRole(), Role.Admin.toString())) {
                LOG.trace("Baning of user -> " + blockaded + "\n " +
                            "by Admin -> " + blocker);
                blockaded.setRole(Role.BanedUser);
                updateUser(blockaded);
                connection.commit();
                return true;
            } else {
                LOG.warn("Access denied! User try to call operation without access permission. User -> " + blocker);
                throw new AppException(Messages.ERR_ACCESS_DENIED);
            }


        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    /**
     * UnBaning of user by admin
     * @param blocker - object of UsesS entity
     *                object should have minimum set of fields
     *                there are:
     *                - id or name
     *
     *                blocker selecting performs firstly by id
     *                if id is null selecting performs by name
     * @param blockaded - object of UsesS entity
     *                  object should have minimum set of fields
     *                there are:
     *                - id or name
     *
     *                blocker selecting performs firstly by id
     *                if id is null selecting performs by name
     * @return true - if operation succeeded
     *         false - if operation failed
     *         null - if were thrown unexpected exceptions
     * @throws AppException
     */
    @Override
    public Boolean unBanUser(UserServiceEntity blocker, UserServiceEntity blockaded) throws AppException {
        LOG.debug("Calling UnBanUser method of " + blocker + " -> " + blockaded);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        User blockerUser = blocker.getUser();
        UserTranslate blockerTranslate = blocker.getDefaultTranslate();

        User blockadedUser = blockaded.getUser();
        UserTranslate blockadedTranslate = blockaded.getDefaultTranslate();

        try {
            connection.setAutoCommit(false);
            LOG.trace("Data validation!");

            if ((blockerUser.getIdUser() == null
                    || blockerUser.getIdUser() < 0)
                    && (blockerTranslate == null
                    || blockerTranslate.getFirstName() == null
                    || blockerTranslate.getFirstName().length() < 2
                    || blockerTranslate.getSecondName() == null
                    || blockerTranslate.getSecondName().length() < 2)) {

                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_PERFORM_OPERATION);
            }
            if ((blockadedUser.getIdUser() == null
                    || blockadedUser.getIdUser() < 0)
                        && (blockadedUser == null
                        || blockadedTranslate.getFirstName() == null
                        || blockadedTranslate.getFirstName().length() < 2
                        || blockadedTranslate.getSecondName() == null
                        || blockadedTranslate.getSecondName().length() < 2)) {

                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_PERFORM_OPERATION);
            }


            LOG.trace("Logic proceeding...");

            if (blockerUser.getIdUser() == null
                    || blockerUser.getIdUser() < 0) {
                blocker = getUser(blockerTranslate.getFirstName(),blockerTranslate.getSecondName());
            } else {
                blocker = getUser(blockerUser.getIdUser());
            }


            if (blockadedUser.getIdUser() == null
                    || blockadedUser.getIdUser() < 0) {
                blockaded = getUser(blockadedTranslate.getFirstName(),blockadedTranslate.getSecondName());
            } else {
                blockaded = getUser(blockadedUser.getIdUser());
            }



            if (Objects.equals(blocker.getRole(), Role.Admin.toString())) {
                LOG.trace("UnBaning of user -> " + blockaded + "\n " +
                        "by Admin -> " + blocker);
                blockaded.setRole(Role.User);
                updateUser(blockaded);
                connection.commit();
                return true;
            } else {
                LOG.warn("Access denied! User try to call operation without access permission. User -> " + blocker);
                throw new AppException(Messages.ERR_ACCESS_DENIED);
            }

        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    /**
     * for insertion entity required minimum set of fields
     *               there are:
     *               - first name
     *               - second name
     *               - role
     *               - registerDate
     *
     * @param userEntry - insertable entity
     * @return entity which has been inserted into database
     */
    @Override
    public UserServiceEntity createUser(UserServiceEntity userEntry) throws AppException {
        LOG.debug("Calling of create method for user -> " + userEntry);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        Account account = null;
        User user = null;
        Language language = null;
        UserTranslate userTranslate = null;
        String login = userEntry.getLogin();
        String password = userEntry.getPassword();

        UserServiceEntity result = null;

        try {
            connection.setAutoCommit(false);
            LOG.trace("Data validation!");
            String role = userEntry.getRole();
            Date date = userEntry.getRegisterDate();
            Map<Locale, UserTranslate> userTranslates = userEntry.getTranslates();
            if (role == null
                    || Role.valueOf(role) == null
                    || date == null
                    || date.getTime() > System.currentTimeMillis()
                    || userTranslates == null
                    || userTranslates.size() < 1
                    || login == null
                    || login.length() < 2
                    || password == null
                    || password.length() < 2) {

                LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
            }
            for (Map.Entry<Locale, UserTranslate> pair : userTranslates.entrySet()) {
                String firstName = pair.getValue().getFirstName();
                String secondName = pair.getValue().getSecondName();

                // checking if exist
                // if not - get method throw exception
                languageDAO.get(pair.getKey().name(), connection);

                if (firstName == null
                        || firstName.length() < 2
                        || secondName == null
                        || secondName.length() < 2) {

                    LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                    throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
                }

            }

            LOG.trace("Creation proceeding...");

            account = new Account();
            account.setIdAccount(getAccountUniqueId());
            account.setBalance(0D);
            account = accountDAO.create(account, connection);


            user = new User();
            user.setRole(userEntry.getRole());
            user.setRegisterDate(date);
            user.setBirthDate(userEntry.getBirthDate());
            user.setGender(userEntry.getGender());
            user.setLogin(login);
            user.setPassword(password);

            user.setRef_account(account.getIdAccount());
            user = userDAO.create(user, connection);

            for (Map.Entry<Locale, UserTranslate> pair : userTranslates.entrySet()) {

                language = languageDAO.get(pair.getKey().name(), connection);
                userTranslate = new UserTranslate();
                userTranslate.setFirstName(pair.getValue().getFirstName());
                userTranslate.setSecondName(pair.getValue().getSecondName());
                userTranslate.setRef_lang(language.getIdlang());
                userTranslate.setRef_user(user.getIdUser());
                userTranslate = userTranslateDAO.create(userTranslate, connection);
                userTranslates.put(pair.getKey(), userTranslate);

            }

            result = getUser(user.getIdUser());
            connection.commit();
            return result;

        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    @Override
    public UserServiceEntity updateUser(UserServiceEntity userServiceEntity) throws AppException {
        LOG.debug("Calling of create method for user -> " + userServiceEntity);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        User user = userServiceEntity.getUser();
        Account account = userServiceEntity.getAccount();
        Map<Locale, UserTranslate> userTranslates = userServiceEntity.getTranslates();
        String login = user.getLogin();
        String password = user.getPassword();

        try {
            connection.setAutoCommit(false);
            LOG.trace("Data validation");
            if (user.getIdUser() == null
                    || user.getIdUser() < 0
                    || user.getRole() == null
                    || (!Objects.equals(user.getRole(), Role.User.toString())
                        && !Objects.equals(user.getRole(), Role.Admin.toString())
                        && !Objects.equals(user.getRole(), Role.BanedUser.toString()))
                    || user.getRef_account() == null
                    || user.getRef_account().length() < 5
                    || user.getRegisterDate() == null
                    || login == null
                    || login.length() < 2
                    || password == null
                    || password.length() < 2) {
                LOG.warn("Data validation was failed");
                throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
            }
            if (user.getGender() == null
                    || (!Objects.equals(user.getGender(), Gender.Male.toString())
                        && !Objects.equals(user.getGender(), Gender.Female.toString()))) {
                user.setGender(null);
            }


            for (Map.Entry<Locale, UserTranslate> pair : userTranslates.entrySet()) {
                String firstName = pair.getValue().getFirstName();
                String secondName = pair.getValue().getSecondName();

                // checking if language exist
                // if not - get method throw exception
                languageDAO.get(pair.getKey().name(), connection);

                if (firstName == null
                        || firstName.length() < 2
                        || secondName == null
                        || secondName.length() < 2) {

                    LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                    throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
                }
                if (account.getIdAccount() == null
                        || account.getIdAccount().length() == 0
                        || account.getBalance() == null
                        || account.getBalance() < 0) {
                    LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                    throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());

                }
               /* try {

                    // checking for existence (if not exist - trow exception)
                    userTranslateDAO.get(firstName, secondName);

                } catch (AppException e) {
                    UserTranslate translate = new UserTranslate();
                    translate.setRef_user(user.getIdUser());
                    translate.setRef_lang(languageDAO.get(pair.getKey().name()).getIdlang());
                    translate.setFirstName(firstName);
                    translate.setSecondName(secondName);
                    translate = userTranslateDAO.create(translate);

                    //pair.setValue(translate);
                    userTranslates.put(pair.getKey(),translate);
                } catch (Throwable t) {
                    throw t;
                }*/
            }
            LOG.trace("Logic proceeding...");

            accountDAO.update(account, connection);
            userDAO.update(user, connection);
            for (Map.Entry<Locale, UserTranslate> pair : userTranslates.entrySet()) {
                userTranslateDAO.update(pair.getValue(), connection);
            }
            connection.commit();
            return getUser(user.getIdUser());

        } catch (AppException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }
    }


}
