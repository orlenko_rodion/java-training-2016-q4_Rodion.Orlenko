package ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common;


import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.sql.Statement;

public interface Accessor {
    Connection getConnection() throws DBException;
    void closeConnection(Connection con);
    void closeStatment(Statement statement);
}
