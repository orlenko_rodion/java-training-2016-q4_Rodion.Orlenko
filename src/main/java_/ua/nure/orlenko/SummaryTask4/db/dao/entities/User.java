package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

import java.sql.Date;

public class User implements Entity{
  private Long idUser;
  private String gender;
  private java.sql.Date registerDate;
  private java.sql.Date birthDate;
  private String role;
  private String ref_account;
  private String password;
  private String login;

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public Date getRegisterDate() {
    return registerDate;
  }

  public void setRegisterDate(Date registerDate) {
    this.registerDate = registerDate;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getRef_account() {
    return ref_account;
  }

  public void setRef_account(String ref_account) {
    this.ref_account = ref_account;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (idUser != null ? !idUser.equals(user.idUser) : user.idUser != null) return false;
    if (gender != null ? !gender.equals(user.gender) : user.gender != null) return false;
    if (!registerDate.equals(user.registerDate)) return false;
    if (birthDate != null ? !birthDate.equals(user.birthDate) : user.birthDate != null) return false;
    if (!role.equals(user.role)) return false;
    if (ref_account != null ? !ref_account.equals(user.ref_account) : user.ref_account != null) return false;
    if (!password.equals(user.password)) return false;
    return login.equals(user.login);
  }

  @Override
  public int hashCode() {
    int result = idUser != null ? idUser.hashCode() : 0;
    result = 31 * result + (gender != null ? gender.hashCode() : 0);
    result = 31 * result + registerDate.hashCode();
    result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
    result = 31 * result + role.hashCode();
    result = 31 * result + (ref_account != null ? ref_account.hashCode() : 0);
    result = 31 * result + password.hashCode();
    result = 31 * result + login.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "User{" +
            "idUser=" + idUser +
            ", gender='" + gender + '\'' +
            ", registerDate=" + registerDate +
            ", birthDate=" + birthDate +
            ", role='" + role + '\'' +
            ", ref_account='" + ref_account + '\'' +
            ", password='" + password + '\'' +
            ", login='" + login + '\'' +
            '}';
  }
}
