package ua.nure.orlenko.SummaryTask4.db.service;

import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.util.List;

/**
 *
 */
public interface UserService {
    UserServiceEntity createUser(UserServiceEntity user) throws AppException;

    UserServiceEntity updateUser(UserServiceEntity userServiceEntity) throws AppException;

    UserServiceEntity getUser(String firstName, String secondName) throws AppException;
    UserServiceEntity loginUser(String login, String password) throws AppException;
    UserServiceEntity getUser(Long id) throws AppException;

    List<UserServiceEntity> getAll() throws AppException;

    Boolean banUser (UserServiceEntity blocker, UserServiceEntity blockaded) throws AppException;
    Boolean unBanUser(UserServiceEntity blocker, UserServiceEntity blockaded) throws AppException;
}
