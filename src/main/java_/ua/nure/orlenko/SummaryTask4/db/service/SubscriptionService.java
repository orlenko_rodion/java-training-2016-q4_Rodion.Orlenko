package ua.nure.orlenko.SummaryTask4.db.service;

import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.service.entities.SubscriptionServiceEntity;

import java.util.List;

/**
 *
 */
public interface SubscriptionService {

    SubscriptionServiceEntity createSubscription(UserServiceEntity user, EditionServiceEntity edition, Integer period) throws AppException;

    SubscriptionServiceEntity getSubscription(UserServiceEntity userServiceEntity, EditionServiceEntity editionServiceEntity);
    List<SubscriptionServiceEntity> getUserSubscriptions(UserServiceEntity user) throws AppException;
    List<SubscriptionServiceEntity> getEditionSubscriptions(EditionServiceEntity user) throws AppException;
}
