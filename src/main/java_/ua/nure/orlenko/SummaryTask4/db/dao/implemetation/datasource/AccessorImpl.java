package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.datasource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Accessor;
import ua.nure.orlenko.SummaryTask4.exception.DBException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/** AccessorImpl class
 *  provides access to database using apache database connection pool with
 *  connection to mysql database
 */
public class AccessorImpl implements Accessor {

    /**
     * apache data base connection pool
     */
    private BasicDataSource dataSource = null;
    /**
     * singleton
     */
    private static AccessorImpl singletonAccessorImpl = null;
    /**
     * logger
     */
    private static final Logger LOG = Logger.getLogger(AccessorImpl.class);

/*    public static void main(String[] args) {
        try {
            AccessorImpl acc = AccessorImpl.getInstance();
            propertyStatement(acc.getConnection());
            acc.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
public AccessorImpl() throws DBException {
        try {
            dataSource = new BasicDataSource();

            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl("jdbc:mysql://localhost:3306/periodicals");
            dataSource.setUsername("root");
            dataSource.setPassword("litkevich");

            dataSource.setValidationQuery("Select 1");
            dataSource.setMaxActive(30);
            dataSource.setInitialSize(10);
            dataSource.setMaxIdle(20);
            dataSource.setMaxWait(9000);

            dataSource.setDefaultAutoCommit(false);

            LOG.trace("Data source ->" + dataSource);
        } catch (Exception e) {
            LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
            throw new DBException(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
        }
    }

    public static synchronized AccessorImpl getInstance()
            throws DBException {
        if (singletonAccessorImpl == null) {
            LOG.info("First instantiating of AccessorImpl object ...");
            singletonAccessorImpl = new AccessorImpl();
        }
        return singletonAccessorImpl;
    }

    public Connection getConnection() throws DBException {
        Connection con = null;
            try {
                LOG.debug("Getting connection object from datasource -> " + dataSource);
                con = dataSource.getConnection();
            } catch (SQLException e) {
                LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
                throw new DBException(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
            }
            return con;
    }

    public void closeConnection(Connection con){
        try {
            if (con != null) {

                con.close();
            }
        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
        }
    }
    public void closeStatment(Statement statement){
        try {
            if (statement != null) {
                statement.close();
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
        }
    }



    public static void propertyStatement(Connection con) throws DBException {

        if (con != null) {
            boolean ro = false;
            try {
                ro = con.getMetaData().supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY);
                System.out.println("TYPE_FORWARD_ONLY - " + ro);

                ro = con.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
                System.out.println("TYPE_SCROLL_INSENSITIVE - " + ro);

                ro = con.getMetaData().supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE);
                System.out.println("TYPE_SCROLL_SENSITIVE - " + ro);

                ro = con.getMetaData().supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                System.out.println("CONCUR_READ_ONLY - " + ro);

                ro = con.getMetaData().supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                System.out.println("CONCUR_UPDATABLE - " + ro);
            } catch (SQLException e) {
                LOG.error(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
                throw new DBException(Messages.ERR_CANNOT_OBTAIN_CONNECTION,e);
            }
        }
    }


}
