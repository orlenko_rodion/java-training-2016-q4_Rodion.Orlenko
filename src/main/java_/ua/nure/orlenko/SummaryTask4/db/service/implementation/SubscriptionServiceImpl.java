package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Subscription;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Accessor;
import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.SubscriptionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.exception.DBException;
import ua.nure.orlenko.SummaryTask4.exception.Messages;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.SubscriptionDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.service.SubscriptionService;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class SubscriptionServiceImpl extends ServiceImpl implements SubscriptionService {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(SubscriptionServiceImpl.class);

    private SubscriptionDAO subscriptionDAO;

    private EditionService editionService;
    private UserService userService;

    public SubscriptionServiceImpl(Accessor accessor) throws DBException {
        super(accessor);
        subscriptionDAO = DaoFactory.getFactory(DaoFactory.RDB).getSubscriptionDAO();

        this.editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();
        this.userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
    }

    public SubscriptionServiceImpl(DaoFactory daoFactory, ServiceFactory serviceFactory, Accessor accessor) throws DBException {
        super(accessor);
        LOG.debug("Setting new DaoFactory");

        this.subscriptionDAO = daoFactory.getSubscriptionDAO();
        this.editionService = serviceFactory.getEditionService();
        this.userService = serviceFactory.getUserService();
    }

    @Override
    public SubscriptionServiceEntity createSubscription(UserServiceEntity user, EditionServiceEntity edition, Integer period) throws AppException {

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        try {
            connection.setAutoCommit(false);

            if (user == null
                    || edition == null
                    || period == null
                    || period < 1) {
                LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED);
            }
            user = userService.loginUser(user.getLogin(), user.getPassword());
            edition = editionService.getEdition(edition.getDefaultTranslate().getTitle());

            if (user.getAccount().getBalance() < (period * edition.getPrice())) {
                throw new AppException("Low balance on yor account for subscription on this edition!");
            }
            user.setAccountBalance(user.getAccountBalance() - (period * edition.getPrice()));
            userService.updateUser(user);

            Subscription subscription = new Subscription();
            subscription.setCost(period * edition.getPrice());
            subscription.setSubscrdate(new Date(System.currentTimeMillis()));
            subscription.setPeriod(period.longValue());
            subscription.setRef_edition(edition.getEdition().getIdEdition());
            subscription.setRef_user(user.getUser().getIdUser());

            subscription = subscriptionDAO.create(subscription, connection);

            SubscriptionServiceEntity subscriptionServiceEntity = new SubscriptionServiceEntity();
            subscriptionServiceEntity.setEdition(edition);
            subscriptionServiceEntity.setSubscription(subscription);
            subscriptionServiceEntity.setUser(user);
            connection.commit();
            return subscriptionServiceEntity;

        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        } catch (AppException e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    @Override
    public SubscriptionServiceEntity getSubscription(UserServiceEntity userServiceEntity, EditionServiceEntity editionServiceEntity) {
        return null;
    }


    @Override
    public List<SubscriptionServiceEntity> getUserSubscriptions(UserServiceEntity user) throws AppException {
        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        List<Subscription> list = subscriptionDAO.getUserSubscription(user.getUser().getIdUser(), connection);
        List<SubscriptionServiceEntity> result = new LinkedList<>();

        try {
            connection.setAutoCommit(false);
            for (Subscription subscription : list) {
                UserServiceEntity userServiceEntity = userService.getUser(subscription.getRef_user());
                EditionServiceEntity editionServiceEntity = editionService.getEdition(subscription.getRef_edition());

                SubscriptionServiceEntity subscriptionServiceEntity = new SubscriptionServiceEntity();
                subscriptionServiceEntity.setSubscription(subscription);
                subscriptionServiceEntity.setUser(userServiceEntity);
                subscriptionServiceEntity.setEdition(editionServiceEntity);

                result.add(subscriptionServiceEntity);
            }
            connection.commit();
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        } catch (AppException e) {
            throw new AppException(e.getMessage(), e);
        }

        return result;
    }

    @Override
    public List<SubscriptionServiceEntity> getEditionSubscriptions(EditionServiceEntity editionServiceEntity) throws AppException {
        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        List<Subscription> list = subscriptionDAO.getEditionSubscription(editionServiceEntity.getEdition().getIdEdition(), connection);
        List<SubscriptionServiceEntity> result = new LinkedList<>();

        try {
            connection.setAutoCommit(false);
            for (Subscription subscription : list) {
                UserServiceEntity userServiceEntity = userService.getUser(subscription.getRef_user());
                EditionServiceEntity editionSS = editionService.getEdition(subscription.getRef_edition());

                SubscriptionServiceEntity subscriptionServiceEntity = new SubscriptionServiceEntity();
                subscriptionServiceEntity.setSubscription(subscription);
                subscriptionServiceEntity.setUser(userServiceEntity);
                subscriptionServiceEntity.setEdition(editionSS);

                result.add(subscriptionServiceEntity);
            }
            connection.commit();
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        } catch (AppException e) {
            throw new AppException(e.getMessage(), e);
        }

        return result;
    }
}
