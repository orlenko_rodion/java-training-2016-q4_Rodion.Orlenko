package ua.nure.orlenko.SummaryTask4.db.service.implementation;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Language;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Accessor;
import ua.nure.orlenko.SummaryTask4.db.service.SubscriptionService;
import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.SubscriptionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.Messages;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.Edition;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.EditionTranslate;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl.factory.DaoFactory;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionTranslateDAO;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.LanguageDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * business logic
 * realization of EditionService
 */
public class EditionServiceImpl extends ServiceImpl implements EditionService {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(EditionServiceImpl.class);

    private EditionDAO editionDAO;
    private EditionTranslateDAO editionTranslateDAO;
    private LanguageDAO languageDAO;


    /**
     * constructor which is used for setting of DB connection strategy
     * @param accessor - implementor of {@link Accessor} interface
     * @throws DBException
     */
    public EditionServiceImpl(Accessor accessor) throws DBException {
        super(accessor);
        editionDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionDAO();
        editionTranslateDAO = DaoFactory.getFactory(DaoFactory.RDB).getEditionTranslateDAO();
        languageDAO = DaoFactory.getFactory(DaoFactory.RDB).getLanguageDao();
    }

    public EditionServiceImpl(DaoFactory factory, Accessor accessor) throws DBException {
        super(accessor);
        LOG.info("Setting new DaoFactory!");
        this.editionDAO = factory.getEditionDAO();
        this.editionTranslateDAO = factory.getEditionTranslateDAO();
        this.languageDAO = factory.getLanguageDao();
    }

    /**
     * for insertion entity required minimum set of fields
     *               there are:
     *               - price
     *               - title
     *               - genre
     *
     * @param editionEntry - insertable entity
     * @return entity which has been inserted into database
     */
    @Override
    public EditionServiceEntity createEdition(EditionServiceEntity editionEntry) throws AppException {
        LOG.debug("Calling of create method for Edition -> " + editionEntry);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        Edition edition = null;
        Language language = null;
        EditionTranslate editionTranslate = null;

        EditionServiceEntity result = null;

        try {
            connection.setAutoCommit(false);

            LOG.trace("Data validation!");
            Double price = editionEntry.getPrice();
            Map<Locale, EditionTranslate> editionTranslates = editionEntry.getTranslates();
            if (price == null
                    || price < 0
                    || editionTranslates == null
                    || editionTranslates.size() < 1) {

                LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
            }
            for (Map.Entry<Locale, EditionTranslate> pair : editionTranslates.entrySet()) {
                String title = pair.getValue().getTitle();
                String genre = pair.getValue().getGenre();

                // checking if exist
                // if not - get method throw exception
                languageDAO.get(pair.getKey().name(), connection);

                if (title == null
                        || title.length() < 2
                        || genre == null
                        || genre.length() < 2) {

                    LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                    throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
                }
                boolean existFlag = false;
                try {

                    // checking if not exist
                    editionTranslateDAO.get(title, connection);

                    LOG.warn(Messages.ERR_CANNOT_CREATE_ENTITY);
                    existFlag = true;
                    throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
                } catch (AppException e) {
                    // It indicates that no such entity in database

                    if (existFlag) {
                        throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
                    }
                }
            }

            LOG.trace("Creation proceeding...");


            edition = new Edition();
            edition.setPrice(editionEntry.getPrice());
            edition.setPublicationDate(editionEntry.getPublicationDate());
            edition = editionDAO.create(edition, connection);

            for (Map.Entry<Locale, EditionTranslate> pair : editionTranslates.entrySet()) {

                language = languageDAO.get(pair.getKey().name(), connection);
                editionTranslate = new EditionTranslate();
                editionTranslate.setTitle(editionEntry.getTitle(pair.getKey()));
                editionTranslate.setAuthor(editionEntry.getAuthor(pair.getKey()));
                editionTranslate.setPublisher(editionEntry.getPublisher(pair.getKey()));
                editionTranslate.setGenre(editionEntry.getGenre(pair.getKey()));
                editionTranslate.setTopic(editionEntry.getTopic(pair.getKey()));

                editionTranslate.setRef_edition(edition.getIdEdition());
                editionTranslate.setRef_lang(language.getIdlang());

                editionTranslate = editionTranslateDAO.create(editionTranslate, connection);
                editionTranslates.put(pair.getKey(), editionTranslate);

            }
            connection.commit();

            result = getEdition(edition.getIdEdition());
            return result;

        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            LOG.error(e.getMessage(),e);
            throw new AppException(e.getMessage(), e);
        } finally {
            getAccessor().closeConnection(connection);
        }
    }

    @Override
    public EditionServiceEntity updateEdition(EditionServiceEntity editionEntry) throws AppException {
        LOG.debug("Calling of create method for edition -> " + editionEntry);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        Edition edition = editionEntry.getEdition();
        Map<Locale, EditionTranslate> editionTranslates = editionEntry.getTranslates();

        try {
            connection.setAutoCommit(false);

            LOG.trace("Data validation");
            if (edition.getIdEdition() == null
                    || edition.getIdEdition() < 0
                    || edition.getPrice() == null
                    || edition.getPrice() < 0) {
                LOG.warn("Data validation was failed");
                throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
            }


            for (Map.Entry<Locale, EditionTranslate> pair : editionTranslates.entrySet()) {
                String title = pair.getValue().getTitle();
                String genre = pair.getValue().getGenre();

                // checking if language exist
                // if not - get method throw exception
                languageDAO.get(pair.getKey().name(), connection);

                if (title == null
                        || title.length() < 2
                        || genre == null
                        || genre.length() < 2) {

                    LOG.warn(Messages.ERR_DATA_VALIDATION_FAILED);
                    throw new AppException(Messages.ERR_DATA_VALIDATION_FAILED, new IllegalArgumentException());
                }
                try {

                    // checking for existence (if not exist - trow exception)
                    editionTranslateDAO.get(title, connection);

                } catch (AppException e) {
                    EditionTranslate translate = new EditionTranslate();
                    translate.setRef_edition(edition.getIdEdition());
                    translate.setRef_lang(languageDAO.get(pair.getKey().name(), connection).getIdlang());
                    translate.setTitle(title);
                    translate.setGenre(genre);
                    translate = editionTranslateDAO.create(translate, connection);

                    //pair.setValue(translate);
                    editionTranslates.put(pair.getKey(),translate);
                }
            }
            LOG.trace("Logic proceeding...");

            editionDAO.update(edition, connection);
            for (Map.Entry<Locale, EditionTranslate> pair : editionTranslates.entrySet()) {
                editionTranslateDAO.update(pair.getValue(), connection);
            }
            connection.commit();
            return getEdition(edition.getIdEdition());

        } catch (AppException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        }finally {
            getAccessor().closeConnection(connection);
        }
    }

    @Override
    public Boolean deleteEdition(EditionServiceEntity edition) throws AppException {
        return null;
    }

    @Override
    public EditionServiceEntity getEdition(String title) throws AppException {
        LOG.debug("Calling of get method for title -> " + title);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        Edition edition = null;
        EditionTranslate editionTranslate = null;
        List<EditionTranslate> editionTranslates = null;

        try {
            connection.setAutoCommit(false);
            editionTranslate = editionTranslateDAO.get(title, connection);
            edition = editionDAO.get(editionTranslate.getRef_edition(), connection);

            editionTranslates = editionTranslateDAO.getAllForEdition(edition.getIdEdition(), connection);

            LOG.trace("Data validation");
            if (edition == null
                    || editionTranslates.size() < 1) {
                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

            EditionServiceEntity resEdition = new EditionServiceEntity();
            resEdition.setEdition(edition);

            for (EditionTranslate translate:
                    editionTranslates) {

                Language language = languageDAO.get(translate.getRef_lang(), connection);
                Locale l = Locale.valueOf(language.getName());
                resEdition.addTranslate(l, translate);
            }
            connection.commit();
            return resEdition;
        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            LOG.error(e.getMessage(),e);
            throw new AppException(e.getMessage(), e);
        } finally {
            getAccessor().closeConnection(connection);
        }

    }

    @Override
    public EditionServiceEntity getEdition(Long id) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        Edition edition = null;
        List<EditionTranslate> editionTranslates = null;

        try {
            connection.setAutoCommit(false);

            edition = editionDAO.get(id, connection);
            editionTranslates = editionTranslateDAO.getAllForEdition(edition.getIdEdition(), connection);

            LOG.trace("Data validation");
            if (edition == null
                    || editionTranslates.size() < 1) {
                LOG.warn("Data validation was failed!");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

            EditionServiceEntity resEdition = new EditionServiceEntity();
            resEdition.setEdition(edition);

            for (EditionTranslate translate:
                    editionTranslates) {

                Language language = languageDAO.get(translate.getRef_lang(), connection);
                Locale locale = Locale.valueOf(language.getName());
                resEdition.addTranslate(locale, translate);
            }
            connection.commit();
            return resEdition;
        } catch (AppException e) {
            LOG.error(e.getMessage(),e);
            throw e;
        } catch (SQLException e) {
            throw new AppException(e.getMessage(), e);
        } finally {
            getAccessor().closeConnection(connection);
        }
    }

    @Override
    public List<EditionServiceEntity> getAll() throws AppException {
        SubscriptionService subscriptionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getSubscriptionService();

        Connection connection = getAccessor().getConnection();
        LOG.trace("Obtaining of connection to database -> " + connection);

        List<Edition> tmpList = editionDAO.getAll(connection);
        List<EditionServiceEntity> resList = new LinkedList<>();

        try {
            connection.setAutoCommit(false);

            for (Edition edition: tmpList) {
                EditionServiceEntity ed = getEdition(edition.getIdEdition());

                List<SubscriptionServiceEntity> list = subscriptionService.getEditionSubscriptions(ed);
                ed.setUserSubscr(list.size());
                double sum = 0;
                for (SubscriptionServiceEntity s: list) {
                    sum += (s.getSubscription().getPeriod() * s.getSubscription().getCost());
                }
                ed.setTotalSum(sum);

                resList.add(ed);
            }

            connection.commit();
        } catch (SQLException e) {
            throw new AppException(e.getMessage(),e);
        }

        return resList;
    }

    @Override
    public List<EditionServiceEntity> SortByPrice(List<EditionServiceEntity> list) {

        list.sort(Comparator.comparing(EditionServiceEntity::getPrice));
        return list;
    }

    @Override
    public List<EditionServiceEntity> SortByTitle(List<EditionServiceEntity> list) {

        list.sort(Comparator.comparing(o -> o.getDefaultTranslate().getTitle()));
        return list;
    }

    @Override
    public List<EditionServiceEntity> SortByGenre(List<EditionServiceEntity> list) {
        return null;
    }


}
