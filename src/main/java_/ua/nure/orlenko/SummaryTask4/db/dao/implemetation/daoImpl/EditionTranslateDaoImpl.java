package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.exception.Messages;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.EditionTranslate;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.EditionTranslateDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.exception.DBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * class-implementor of LanguageDAO interface
 * provides an access to DB information (mostly throw the CRUD methods)
 */
public class EditionTranslateDaoImpl implements EditionTranslateDAO {
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(EditionTranslateDaoImpl.class);


    /**
     *
     * @param entity insertable entity
     *                 for insertion entity required minimum set of fields
     *               there are:
     *               - genre
     *               - title
     *               - refEdition
     *               - refLang
     * @return inserted entity
     * @throws AppException
     */
    @Override
    public EditionTranslate create(EditionTranslate entity, Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "INSERT INTO edition_translate(Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang) VALUES(?,?,?,?,?,?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getAuthor());
            statement.setString(2, entity.getPublisher());
            statement.setString(3, entity.getGenre());
            statement.setString(4, entity.getTopic());
            statement.setString(5, entity.getTitle());
            statement.setLong(6, entity.getRef_edition());
            statement.setLong(7, entity.getRef_lang());

            int rowsInserted = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsInserted > 0) {
                LOG.info("A new UserTranslate was inserted successfully!");
                sql = "SELECT Id_EditionTr, Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang\n" +
                        "FROM edition_translate \n" +
                        "WHERE Title = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setString(1, entity.getTitle());
                ResultSet result = statement.executeQuery();

                if (result.next()){
                    EditionTranslate editionTranslate = new EditionTranslate();
                    editionTranslate.setId_editiontr(result.getLong(1));
                    editionTranslate.setAuthor(result.getString(2));
                    editionTranslate.setPublisher(result.getString(3));
                    editionTranslate.setGenre(result.getString(4));
                    editionTranslate.setTopic(result.getString(5));
                    editionTranslate.setTitle(result.getString(6));
                    editionTranslate.setRef_edition(result.getLong(7));
                    editionTranslate.setRef_lang(result.getLong(8));
                    connection.setAutoCommit(defaultAutoCommit);
                    return editionTranslate;
                }
                return null;
            } else {
                LOG.warn("A new UserTranslate insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }
        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public EditionTranslate get(String title, Connection connection) throws AppException {
        LOG.debug("Calling of get method for title -> " + title);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT Id_EditionTr, Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang" +
                    " FROM edition_translate" +
                    " WHERE Title = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,title);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            EditionTranslate editionTranslate = null;
            if (result.next()) {

                editionTranslate = new EditionTranslate();
                editionTranslate.setId_editiontr(result.getLong(1));
                editionTranslate.setAuthor(result.getString(2));
                editionTranslate.setPublisher(result.getString(3));
                editionTranslate.setGenre(result.getString(4));
                editionTranslate.setTopic(result.getString(5));
                editionTranslate.setTitle(result.getString(6));
                editionTranslate.setRef_edition(result.getLong(7));
                editionTranslate.setRef_lang(result.getLong(8));

                LOG.trace("Selecting UserTranslate entity from database wse succeeded! ");
                return editionTranslate;
            } else {
                LOG.warn("Selecting UserTranslate entity from database was failed");
                throw new AppException(Messages.ERR_ILLEGAL_ARGUMENT);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<EditionTranslate> getAllForEdition(Long idEdition, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + idEdition);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT Id_EditionTr, Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang\n" +
                         "FROM edition_translate\n" +
                         "WHERE Ref_Edition = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,idEdition);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            List<EditionTranslate> list = new ArrayList();
            while (result.next()) {
                EditionTranslate editionTranslate = new EditionTranslate();
                editionTranslate.setId_editiontr(result.getLong(1));
                editionTranslate.setAuthor(result.getString(2));
                editionTranslate.setPublisher(result.getString(3));
                editionTranslate.setGenre(result.getString(4));
                editionTranslate.setTopic(result.getString(5));
                editionTranslate.setTitle(result.getString(6));
                editionTranslate.setRef_edition(result.getLong(7));
                editionTranslate.setRef_lang(result.getLong(8));

                list.add(editionTranslate);
            }
                LOG.trace("Selecting EditionTranslate entity from database wse succeeded!");
                return list;

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(String title, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for title -> " + title);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM edition_translate" +
                        " WHERE Title = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,title);

            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("EditionTranslate was deleted successfully!");
                return true;
            } else {
                LOG.warn("EditionTranslate deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM edition_translate WHERE Id_EditionTr = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);

            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("EditionTranslate was deleted successfully!");
                return true;
            } else {
                LOG.warn("EditionTranslate deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public EditionTranslate get(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT Id_EditionTr, Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang" +
                        " FROM edition_translate" +
                        " WHERE Id_EditionTr = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            EditionTranslate editionTranslate = null;
            if (result.next()) {

                editionTranslate = new EditionTranslate();
                editionTranslate.setId_editiontr(result.getLong(1));
                editionTranslate.setAuthor(result.getString(2));
                editionTranslate.setPublisher(result.getString(3));
                editionTranslate.setGenre(result.getString(4));
                editionTranslate.setTopic(result.getString(5));
                editionTranslate.setTitle(result.getString(6));
                editionTranslate.setRef_edition(result.getLong(7));
                editionTranslate.setRef_lang(result.getLong(8));

                LOG.trace("Selecting UserTranslate entity from database wse succeeded!");
                return editionTranslate;
            } else {
                LOG.warn("Selecting UserTranslate entity from database was failed!");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public EditionTranslate update(EditionTranslate entity, Connection connection) throws AppException {
        LOG.debug("Calling of update method for entity -> " + entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE edition_translate " +
                        " SET Author = ?, Publisher =?, Genre = ?," +
                            " Topic = ?, Title = ?, Ref_Edition = ?, Ref_Lang = ? " +
                        " WHERE Id_EditionTr = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getAuthor());
            statement.setString(2, entity.getPublisher());
            statement.setString(3, entity.getGenre());
            statement.setString(4, entity.getTopic());
            statement.setString(5, entity.getTitle());
            statement.setLong(6, entity.getRef_edition());
            statement.setLong(7, entity.getRef_lang());

            statement.setLong(8, entity.getId_editiontr());

            int rowsUpdated = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsUpdated > 0) {
                LOG.info("A new EditionTranslate was updated successfully!");
                sql = "SELECT Id_EditionTr, Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang\n" +
                        "FROM edition_translate \n" +
                        "WHERE Title = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setString(1, entity.getTitle());

                ResultSet result = statement.executeQuery();
                if (result.next()){
                    EditionTranslate editionTranslate = new EditionTranslate();
                    editionTranslate.setId_editiontr(result.getLong(1));
                    editionTranslate.setAuthor(result.getString(2));
                    editionTranslate.setPublisher(result.getString(3));
                    editionTranslate.setGenre(result.getString(4));
                    editionTranslate.setTopic(result.getString(5));
                    editionTranslate.setTitle(result.getString(6));
                    editionTranslate.setRef_edition(result.getLong(7));
                    editionTranslate.setRef_lang(result.getLong(8));
                    connection.setAutoCommit(defaultAutoCommit);
                    return editionTranslate;
                }
                return null;
            } else {
                LOG.warn("A editionTranslate updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new DBException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<EditionTranslate> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT Id_EditionTr, Author, Publisher, Genre, Topic, Title, Ref_Edition, Ref_Lang\n" +
                         "FROM edition_translate";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<EditionTranslate> list = new ArrayList();
            while (result.next()) {
                EditionTranslate editionTranslate = new EditionTranslate();
                editionTranslate.setId_editiontr(result.getLong(1));
                editionTranslate.setAuthor(result.getString(2));
                editionTranslate.setPublisher(result.getString(3));
                editionTranslate.setGenre(result.getString(4));
                editionTranslate.setTopic(result.getString(5));
                editionTranslate.setTitle(result.getString(6));
                editionTranslate.setRef_edition(result.getLong(7));
                editionTranslate.setRef_lang(result.getLong(8));

                list.add(editionTranslate);
            }
            LOG.trace("Selecting EditionTranslate entity from database was succeeded!");
            return list;


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);
        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}
