package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

import java.sql.Date;

/**
 * POJO class
 * reflect certain DB dao
 */
public class Subscription implements Entity {
  private Long idsubscr;
  private java.sql.Date subscrdate;
  private Long period;
  private Double cost;
  private Long ref_user;
  private Long ref_edition;

  public Subscription() {
  }

  public Subscription(Date subscrdate, Long period, Double cost, Long ref_user, Long ref_edition) {
    this.subscrdate = subscrdate;
    this.period = period;
    this.cost = cost;
    this.ref_user = ref_user;
    this.ref_edition = ref_edition;
  }

  public Subscription(Long idsubscr, Date subscrdate, Long period, Double cost, Long ref_user, Long ref_edition) {
    this.idsubscr = idsubscr;
    this.subscrdate = subscrdate;
    this.period = period;
    this.cost = cost;
    this.ref_user = ref_user;
    this.ref_edition = ref_edition;
  }

  public Long getIdsubscr() {
    return idsubscr;
  }

  public void setIdsubscr(Long idsubscr) {
    this.idsubscr = idsubscr;
  }

  public java.sql.Date getSubscrdate() {
    return subscrdate;
  }

  public void setSubscrdate(java.sql.Date subscrdate) {
    this.subscrdate = subscrdate;
  }

  public Long getPeriod() {
    return period;
  }

  public void setPeriod(Long period) {
    this.period = period;
  }

  public Double getCost() {
    return cost;
  }

  public void setCost(Double cost) {
    this.cost = cost;
  }

  public Long getRef_user() {
    return ref_user;
  }

  public void setRef_user(Long ref_user) {
    this.ref_user = ref_user;
  }

  public Long getRef_edition() {
    return ref_edition;
  }

  public void setRef_edition(Long ref_edition) {
    this.ref_edition = ref_edition;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Subscription that = (Subscription) o;

    if (idsubscr != null ? !idsubscr.equals(that.idsubscr) : that.idsubscr != null) return false;
    if (!subscrdate.equals(that.subscrdate)) return false;
    if (!period.equals(that.period)) return false;
    if (!cost.equals(that.cost)) return false;
    if (!ref_user.equals(that.ref_user)) return false;
    return ref_edition.equals(that.ref_edition);
  }

  @Override
  public int hashCode() {
    int result = idsubscr != null ? idsubscr.hashCode() : 0;
    result = 31 * result + subscrdate.hashCode();
    result = 31 * result + period.hashCode();
    result = 31 * result + cost.hashCode();
    result = 31 * result + ref_user.hashCode();
    result = 31 * result + ref_edition.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Subscription{" +
            "id=" + idsubscr +
            ", subscrDate=" + subscrdate +
            ", period=" + period +
            ", cost=" + cost +
            ", ref_user=" + ref_user +
            ", ref_edition=" + ref_edition +
            '}';
  }
}
