package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

/**
 * POJO class
 * reflect certain DB dao
 */
public class Language implements Entity {
  private Long idlang;
  private String name;
  private String description;

  public Language() {
  }

  public Language(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public Language(Long idlang, String name, String description) {
    this.idlang = idlang;
    this.name = name;
    this.description = description;
  }

  public Long getIdlang() {
    return idlang;
  }

  public void setIdlang(Long idlang) {
    this.idlang = idlang;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Language language = (Language) o;

    if (!idlang.equals(language.idlang)) return false;
    if (!name.equals(language.name)) return false;
    return description.equals(language.description);
  }

  @Override
  public int hashCode() {
    int result = idlang.hashCode();
    result = 31 * result + name.hashCode();
    result = 31 * result + description.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Language{" +
            "idlang=" + idlang +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            '}';
  }
}
