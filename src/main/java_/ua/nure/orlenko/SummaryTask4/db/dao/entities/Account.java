package ua.nure.orlenko.SummaryTask4.db.dao.entities;

import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.common.Entity;

/**
 * POJO class
 * reflect certain DB entity
 */
public class Account implements Entity {

  private String idAccount;
  private Double balance;

  public Account(){
  }

  public Account(String idAccount) {
    this.idAccount = idAccount;
    this.balance = 0D;
  }

  public Account(String idAccount, Double balance) {
    this.idAccount = idAccount;
    this.balance = balance;
  }

  public String getIdAccount() {
    return idAccount;
  }

  public void setIdAccount(String idAccount) {
    this.idAccount = idAccount;
  }

  public Double getBalance() {
    return balance;
  }

  public void setBalance(Double balance){
    this.balance = balance;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Account account = (Account) o;

    if (idAccount != null ? !idAccount.equals(account.idAccount) : account.idAccount != null) return false;
    return balance != null ? balance.equals(account.balance) : account.balance == null;
  }

  @Override
  public int hashCode() {
    int result = idAccount != null ? idAccount.hashCode() : 0;
    result = 31 * result + (balance != null ? balance.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Account{" +
            "idAccount='" + idAccount + '\'' +
            ", balance=" + balance +
            '}';
  }
}
