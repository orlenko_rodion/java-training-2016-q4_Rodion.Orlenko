package ua.nure.orlenko.SummaryTask4.db.dao.implemetation.daoImpl;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.exception.Messages;
import ua.nure.orlenko.SummaryTask4.db.dao.entities.UserTranslate;
import ua.nure.orlenko.SummaryTask4.db.dao.interfaces.dao.entities.UserTranslateDAO;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class UserTranslateDaoImpl implements UserTranslateDAO{
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(UserTranslateDaoImpl.class);

    /**
     *
     * @param entity insertable entity
     *               for insertion, entity required all fields except Id
     * @return inserted entity
     * @throws AppException
     */
    @Override
    public UserTranslate create(UserTranslate entity, Connection connection) throws AppException {
        LOG.debug("Calling of create method for entity -> " + entity);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "INSERT INTO user_translate(FirstName, SecondName, Ref_User, Ref_Lang) VALUES(?,?,?,?)";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getFirstName());
            statement.setString(2,entity.getSecondName());
            statement.setLong(3,entity.getRef_user());
            statement.setLong(4,entity.getRef_lang());

            int rowsInserted = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);

            if (rowsInserted > 0) {
                LOG.info("A new UserTranslate was inserted successfully!");
                sql = "SELECT IdUser, FirstName, SecondName, Ref_User, Ref_Lang\n" +
                        "FROM user_translate\n" +
                        "WHERE IdUser = (SELECT MAX(IdUser) FROM user_translate)";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                ResultSet result = statement.executeQuery();
                if (result.next()){
                    UserTranslate userTranslate = new UserTranslate();
                    userTranslate.setIdUser(result.getLong(1));
                    userTranslate.setFirstName(result.getString(2));
                    userTranslate.setSecondName(result.getString(3));
                    userTranslate.setRef_user(result.getLong(4));
                    userTranslate.setRef_lang(result.getLong(5));
                    connection.setAutoCommit(defaultAutoCommit);
                    return userTranslate;
                }
                return null;
            } else {
                LOG.warn("A new UserTranslate insertion was failed!");
                throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY);
            }
        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_CREATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_CREATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public boolean delete(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM user_translate WHERE IdUser = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);

            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("UserTranslate was deleted successfully!");
                return true;
            } else {
                LOG.warn("UserTranslate deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }

    }

    @Override
    public boolean delete(String firstName, String secondName, Connection connection) throws AppException {
        LOG.debug("Calling of delete method for name -> " + firstName + " " + secondName);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);

            String sql = "DELETE FROM user_translate WHERE FirstName = ? AND SecondName = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,firstName);
            statement.setString(1,secondName);

            int rowsProceeded = statement.executeUpdate();
            connection.setAutoCommit(defaultAutoCommit);
            if (rowsProceeded > 0) {
                LOG.info("UserTranslate was deleted successfully!");
                return true;
            } else {
                LOG.warn("UserTranslate deleting was failed!");
                throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_DELETE_ENTITY, e);
            throw new AppException(Messages.ERR_CANNOT_DELETE_ENTITY, e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public UserTranslate get(Long id, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + id);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdUser, FirstName, SecondName, Ref_User, Ref_Lang FROM user_translate WHERE IdUser = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,id);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            UserTranslate userTranslate = null;
            if (result.first()) {

                userTranslate = new UserTranslate();
                userTranslate.setIdUser(result.getLong(1));
                userTranslate.setFirstName(result.getString(2));
                userTranslate.setSecondName(result.getString(3));
                userTranslate.setRef_user(result.getLong(4));
                userTranslate.setRef_lang(result.getLong(5));

                LOG.trace("Selecting UserTranslate entity from database wse succeeded!");
                return userTranslate;
            } else {
                LOG.warn("Selecting UserTranslate entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public UserTranslate get(String firstName, String secondName, Connection connection) throws AppException {
        LOG.debug("Calling of get method for name -> " + firstName + " " + secondName);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdUser, FirstName, SecondName, Ref_User, Ref_Lang" +
                        " FROM user_translate" +
                        " WHERE FirstName = ?" +
                        " AND SecondName = ?";

            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,firstName);
            statement.setString(2,secondName);

            ResultSet result = statement.executeQuery();
            connection.setAutoCommit(defaultAutoCommit);

            UserTranslate userTranslate = null;
            if (result.first()) {

                userTranslate = new UserTranslate();
                userTranslate.setIdUser(result.getLong(1));
                userTranslate.setFirstName(result.getString(2));
                userTranslate.setSecondName(result.getString(3));
                userTranslate.setRef_user(result.getLong(4));
                userTranslate.setRef_lang(result.getLong(5));

                LOG.trace("Selecting UserTranslate entity from database was succeeded!");
                return userTranslate;
            } else {
                LOG.warn("Selecting UserTranslate entity from database was failed");
                throw new AppException(Messages.ERR_CANNOT_GET_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<UserTranslate> getAllForUser(Long idUser, Connection connection) throws AppException {
        LOG.debug("Calling of get method for id -> " + idUser);
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "SELECT IdUser, FirstName, SecondName, Ref_User, Ref_Lang " +
                        " FROM user_translate" +
                        " WHERE Ref_User = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setLong(1,idUser);
            ResultSet result = statement.executeQuery();

            connection.setAutoCommit(defaultAutoCommit);

            List<UserTranslate> list = new ArrayList();
            while (result.next()) {
                UserTranslate userTranslate = new UserTranslate();
                userTranslate.setIdUser(result.getLong(1));
                userTranslate.setFirstName(result.getString(2));
                userTranslate.setSecondName(result.getString(3));
                userTranslate.setRef_user(result.getLong(4));
                userTranslate.setRef_lang(result.getLong(5));

                list.add(userTranslate);
            }
            return list;

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public UserTranslate update(UserTranslate entity, Connection connection) throws AppException {
        LOG.debug("Calling of update method for entity -> " + entity );
        PreparedStatement statement = null;
        boolean defaultAutoCommit = false;

        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String sql = "UPDATE user_translate SET FirstName = ?, SecondName = ?, Ref_User = ?, Ref_Lang = ? " +
                         "WHERE IdUser = ?";
            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.prepareStatement(sql);
            statement.setString(1,entity.getFirstName());
            statement.setString(2,entity.getSecondName());
            statement.setLong(3,entity.getRef_user());
            statement.setLong(4,entity.getRef_lang());
            statement.setLong(5,entity.getIdUser());

            int rowsUpdated = statement.executeUpdate();

            connection.setAutoCommit(defaultAutoCommit);
            if (rowsUpdated > 0) {
                LOG.info("A UserTranslate was updated successfully!");
                sql = "SELECT IdUser, FirstName, SecondName, Ref_User, Ref_Lang FROM user_translate WHERE IdUser = ?";
                LOG.trace("Creating of the statement using obtained connection -->" + connection);
                statement = connection.prepareStatement(sql);
                statement.setLong(1,entity.getIdUser());

                ResultSet result = statement.executeQuery();
                connection.setAutoCommit(defaultAutoCommit);
                if (result.first()){
                    UserTranslate resUserTr = new UserTranslate();
                    resUserTr.setIdUser(result.getLong(1));
                    resUserTr.setFirstName(result.getString(2));
                    resUserTr.setSecondName(result.getString(3));
                    resUserTr.setRef_user(result.getLong(4));
                    resUserTr.setRef_lang(result.getLong(5));
                    connection.setAutoCommit(defaultAutoCommit);
                    return resUserTr;
                }
                return null;
            } else {
                LOG.warn("A userTranslate updating was failed!");
                throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY);
            }

        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_UPDATE_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_UPDATE_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }

    @Override
    public List<UserTranslate> getAll(Connection connection) throws AppException {
        LOG.debug("Calling of getAll method");
        Statement statement = null;
        boolean defaultAutoCommit = false;


        try {
            defaultAutoCommit = connection.getAutoCommit();
            connection.setAutoCommit(true);


            String  sql = "SELECT IdUser, FirstName, SecondName, Ref_User, Ref_Lang FROM user_translate";

            LOG.trace("Creating of the statement using obtained connection -->" + connection);
            statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            connection.setAutoCommit(defaultAutoCommit);

            List<UserTranslate> list = new ArrayList();
            while (result.next()) {
                UserTranslate userTranslate = new UserTranslate();
                userTranslate.setIdUser(result.getLong(1));
                userTranslate.setFirstName(result.getString(2));
                userTranslate.setSecondName(result.getString(3));
                userTranslate.setRef_user(result.getLong(4));
                userTranslate.setRef_lang(result.getLong(5));

                list.add(userTranslate);
            }
            LOG.trace("Selecting User entity from database was succeeded!");
            return list;


        } catch (SQLException e) {
            LOG.error(Messages.ERR_CANNOT_GET_ENTITY,e);
            throw new AppException(Messages.ERR_CANNOT_GET_ENTITY,e);

        } finally {
            try { statement.close(); } catch (SQLException ignored) {}
        }
    }
}
