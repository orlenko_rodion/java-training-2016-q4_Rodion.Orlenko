package ua.nure.orlenko.SummaryTask4.enumeration;

/**
 * Enumeration of locales
 */
public enum Locale {
    RU("Русский"), EN("English");

    String description;
    Locale(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}
