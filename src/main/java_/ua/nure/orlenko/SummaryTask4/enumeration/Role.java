package ua.nure.orlenko.SummaryTask4.enumeration;

/**
 * Enumeration of roles
 */
public enum Role {
    Admin,User,
    /**
     * bad practice(
     * added this field because don't want to do refactor of database
     */
    BanedUser
}
