package ua.nure.orlenko.SummaryTask4.exception;

/**
 * Holder for exception's massages
 */
public interface Messages {
    String ERR_CANNOT_OBTAIN_CONNECTION = "Cannot obtain connection to datasource!";
    String ERR_CANNOT_OBTAIN_DAO = "Cannot obtain data access object!";
    String ERR_CANNOT_OBTAIN_DATASOURCE = "Cannot obtain datasource!";
    String ERR_CANNOT_RETURN_OBJECT = "Cannot return required object";
    String ERR_CANNOT_PERFORM_OPERATION = "Cannot perform this operation";

    String ERR_CANNOT_CREATE_ENTITY = "Failure in entity addition to database!";
    String ERR_CANNOT_DELETE_ENTITY = "Failure in entity deleting from database!";
    String ERR_CANNOT_GET_ENTITY = "Failure in entity selecting from database!";
    String ERR_CANNOT_UPDATE_ENTITY = "Failure in entity updating in database!";

    String ERR_ILLEGAL_ARGUMENT = "Cannot execute query with current arguments!";
    String ERR_DATA_VALIDATION_FAILED = "Data validation was failed!";

    String ERR_ACCESS_DENIED = "No access permission for using this operation!";

    String ERR_LOGIN_FAILED = "Login failed! Wrong login or password!";

}
