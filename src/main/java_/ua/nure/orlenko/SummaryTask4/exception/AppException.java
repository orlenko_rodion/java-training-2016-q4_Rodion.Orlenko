package ua.nure.orlenko.SummaryTask4.exception;

/**
 * Super class of exceptions for application errors
 */
public class AppException extends Exception{

    public AppException() {
        super();
    }

    public AppException(String message){
        super(message);
    }
    public AppException(String message,Throwable caused){
        super(message,caused);
    }
}
