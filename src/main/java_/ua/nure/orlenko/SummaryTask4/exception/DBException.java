package ua.nure.orlenko.SummaryTask4.exception;

/**
 * Exception class for indicating errors
 * caused by work with database
 */
public class DBException extends AppException {
    public DBException() {
        super();
    }

    public DBException(String message) {
        super(message);
    }
    public DBException(String message, Throwable caused) {
        super(message, caused);
    }
}
