package ua.nure.orlenko.SummaryTask4.exception;

/**
 *
 */
public class DaoException extends AppException {
    public DaoException() {
    }

    public DaoException(String message) {
        super(message);
    }
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
