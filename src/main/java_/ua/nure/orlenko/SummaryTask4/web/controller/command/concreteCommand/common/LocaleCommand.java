package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.common;

import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Rodion-PC on 1/30/2017.
 */
public class LocaleCommand extends Command {
    public LocaleCommand() {
    }

    public LocaleCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {
        if ( request.getParameter("locale") == null
                    || request.getParameter("locale").equals("en")) {

            request.getSession().setAttribute("resourceBundle", ResourceBundle.getBundle("Language", new Locale("en","US")));
        }
        if(request.getParameter("locale").equals("ru")) {
            request.getSession().setAttribute("resourceBundle", ResourceBundle.getBundle("Language", new Locale("ru","RU")));
        }
        int i=2;
        Command command = Command.getLast(i);
        while (command != null) {
            if (!(command instanceof LocaleCommand)
                    && !(command instanceof LoginCommand)) {
                return command.execute(request, response);
            }
            i++;
            command = Command.getLast(i);
        }
        return Path.PAGE_INDEX_PAGE;
    }
}
