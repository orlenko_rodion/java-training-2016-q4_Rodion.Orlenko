package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.user;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.service.EditionService;
import ua.nure.orlenko.SummaryTask4.db.service.SubscriptionService;
import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.SubscriptionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

/**
 * Created by Rodion-PC on 1/30/2017.
 */
public class SubscriptionCommand extends Command {
    private static final Logger LOG = Logger.getLogger(SubscriptionCommand.class);
    public SubscriptionCommand() {
    }

    public SubscriptionCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {
        LOG.debug("SubscriptionCommand start");
        ResourceBundle bundle = (ResourceBundle) request.getSession().getAttribute("resourceBundle");

        try {
            UserServiceEntity user = (UserServiceEntity) request.getSession().getAttribute("user");
            LOG.trace("User obtained => " + user);

            if (user == null) {
                throw new AppException(bundle.getString("error.loginFirst"));
            }

            String title = request.getParameter("title");
            String period = request.getParameter("period");
            if (title != null && title.length() > 1 && Integer.parseInt(period) > 0) {
                EditionService editionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getEditionService();
                SubscriptionService subscriptionService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getSubscriptionService();
                EditionServiceEntity edition = editionService.getEdition(title);
                SubscriptionServiceEntity subscriptionServiceEntity = subscriptionService.createSubscription(user,edition,Integer.parseInt(period));

                request.setAttribute("statusMessage",bundle.getString("subscription.addOk"));
            }
        } catch (AppException e) {
            LOG.warn(e.getMessage(), e);
            throw e;
        }
        return Path.PAGE_STATUS_PAGE;
    }
}
