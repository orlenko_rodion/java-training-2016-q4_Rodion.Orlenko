package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;
import ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.admin.*;
import ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.common.*;
import ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.user.SubscriptionCommand;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 */
public class CommandContainer {
    private static final Logger LOG = Logger.getLogger(CommandContainer.class);

    private static Map<String, Command> commands = new TreeMap<>();

    static {
        // common commands
        commands.put("login",new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("locale", new LocaleCommand());
        commands.put("register", new RegisterCommand());
        commands.put("cabinet", new CabinetCommand());
        commands.put("noCommand", new NoCommand());
        commands.put("getAllEditions", new AllEditionsCommand());

        // admin commands
        commands.put("addEdition", new AddEditionCommand());
        commands.put("banUser", new BanUserCommand());
        commands.put("editEdition", new EditEditionCommand());
        commands.put("removeEdition", new RemoveEditionCommand());
        commands.put("unbanUser", new UnbanUserCommand());

        // user command
        commands.put("subscribe", new SubscriptionCommand());
        // FIXME: 1/29/2017 add commands
    }

    public static Command get(String commandName) {
        Command command;
        if (commandName != null) {

            command = commands.get(commandName);
            if (command == null) {
                LOG.warn("Command not found => " + commandName);
                return commands.get("noCommand");
            }
        }
        return commands.get(commandName);
    }
}
