package ua.nure.orlenko.SummaryTask4.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Rodion-PC on 1/30/2017.
 */
public class LocaleFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        ResourceBundle bundle = (ResourceBundle) request.getSession().getAttribute("resourceBundle");
        if (bundle == null) {
            request.getSession().setAttribute("resourceBundle", ResourceBundle.getBundle("Language", new Locale("en","US")));
        }

       /* System.out.println(request.getLocale().toString());
        System.out.println(request.getRequestURL());
        if ( servletRequest.getParameter("locale").equals("en")) {

            request.getSession().setAttribute("resourceBundle", ResourceBundle.getBundle("Language", new Locale("en","US")));
        }
        if(request.getParameter("locale").equals("ru")) {
            request.getSession().setAttribute("resourceBundle", ResourceBundle.getBundle("Language", new Locale("ru","RU")));
        }
*/
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
