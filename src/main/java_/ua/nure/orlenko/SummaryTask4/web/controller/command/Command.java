package ua.nure.orlenko.SummaryTask4.web.controller.command;

import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *  abstract class for Concrete commands
 */
public abstract class Command {
    private ServiceFactory serviceFactory;

    private static List<Command> history = new LinkedList<>();

    public Command() {
        serviceFactory = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE);
    }

    public Command(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    public void setServiceFactory(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public abstract String execute(HttpServletRequest request, HttpServletResponse response) throws AppException;

    public static void putTuHistory(Command command) {
        if(command != null) {
            history.add(command);
        }
        if (history.size() > 20) {
            for (int i = 0; i < 10; i++) {
                history.remove(i);
            }
        }
    }
    public static Command getLast(int fromEnd) {
        if (history.size() > 0 && fromEnd <= history.size()) {
            ListIterator<Command> iterator = history.listIterator(history.size() - 1);

            while (iterator.hasPrevious()) {
                Command command = iterator.previous();
                fromEnd--;
                if (fromEnd == 1) {
                    return command;
                }
            }
        }
        return null;
    }
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
