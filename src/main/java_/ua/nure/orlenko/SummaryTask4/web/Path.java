package ua.nure.orlenko.SummaryTask4.web;

/**
 * Path Holder
 */
public interface Path {
    String PAGE_INDEX_PAGE = "/index.jsp";
    String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
    String PAGE_LIST_EDITIONS_PAGE = "/WEB-INF/jsp/others/listEditions.jsp";
    String PAGE_STATUS_PAGE = "/WEB-INF/jsp/others/status.jsp";

}
