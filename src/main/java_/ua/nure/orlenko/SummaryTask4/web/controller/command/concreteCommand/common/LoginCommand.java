package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.common;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

/**
 * Created by Rodion-PC on 1/29/2017.
 */
public class LoginCommand extends Command {
    private static final Logger LOG = Logger.getLogger(LoginCommand.class);

    public LoginCommand() {
        super();
    }

    public LoginCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {
        LOG.debug("Login command start!");

        try {

            String login = request.getParameter("login");
            String password = request.getParameter("password");

            UserServiceEntity user = getServiceFactory().getUserService().loginUser(login, password);
            LOG.trace("User obtained => " + user);
            request.getSession().setAttribute("user", user);
        } catch (AppException e) {
            LOG.warn(e.getMessage(), e);
            ResourceBundle bundle = (ResourceBundle) request.getSession().getAttribute("resourceBundle");
            throw new AppException(bundle.getString("error.cannotLogin"));
        }
        int i=2;
       /* Command command = Command.getLast(i);
        while (command != null) {
            if (!(command instanceof LoginCommand)
                    && !(command instanceof LocaleCommand)) {
                return command.execute(request, response);
            }
            i++;
            command = Command.getLast(i);
        }*/

        return Path.PAGE_INDEX_PAGE;
    }
}
