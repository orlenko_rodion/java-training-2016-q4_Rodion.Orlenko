package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.common;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Rodion-PC on 1/29/2017.
 */
public class LogoutCommand extends Command {
    private static final Logger LOG = Logger.getLogger(LogoutCommand.class);

    public LogoutCommand() {
        super();
    }

    public LogoutCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {
        LOG.debug("Login command start!");

        request.getSession().setAttribute("user", null);

        return Path.PAGE_INDEX_PAGE;
    }
}
