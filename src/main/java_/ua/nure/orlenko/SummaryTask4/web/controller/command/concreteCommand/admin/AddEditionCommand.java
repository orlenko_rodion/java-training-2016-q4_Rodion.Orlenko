package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.admin;

import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Rodion-PC on 1/29/2017.
 */
public class AddEditionCommand extends Command {

    public AddEditionCommand() {
        super();
    }

    public AddEditionCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {
        return null;
    }
}
