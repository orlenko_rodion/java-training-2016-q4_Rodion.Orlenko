package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.common;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.service.entities.EditionServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Rodion-PC on 1/30/2017.
 */
public class AllEditionsCommand extends Command {
    private static final Logger LOG = Logger.getLogger(AllEditionsCommand.class);
    public AllEditionsCommand() {
    }

    public AllEditionsCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {

        LOG.debug("AllEditions command start!");
        try {

            List<EditionServiceEntity> editions = getServiceFactory().getEditionService().getAll();
            LOG.trace("List obtained => " + editions);
            request.setAttribute("list", editions);



        } catch (AppException e) {
            LOG.warn(e.getMessage(), e);
            ResourceBundle bundle = (ResourceBundle) request.getSession().getAttribute("resourceBundle");
            throw new AppException(bundle.getString("error.cannotGetAllEditions"));
        }

        return Path.PAGE_LIST_EDITIONS_PAGE;
    }
}
