package ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.common;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.db.service.UserService;
import ua.nure.orlenko.SummaryTask4.db.service.entities.UserServiceEntity;
import ua.nure.orlenko.SummaryTask4.db.service.implementation.factory.ServiceFactory;
import ua.nure.orlenko.SummaryTask4.enumeration.Gender;
import ua.nure.orlenko.SummaryTask4.enumeration.Locale;
import ua.nure.orlenko.SummaryTask4.enumeration.Role;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.ResourceBundle;

/**
 * Created by Rodion-PC on 1/30/2017.
 */
public class RegisterCommand extends Command {
    private static final Logger LOG = Logger.getLogger(RegisterCommand.class);
    public RegisterCommand() {
        super();
    }

    public RegisterCommand(ServiceFactory serviceFactory) {
        super(serviceFactory);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws AppException {
        LOG.debug("Login command start!");

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String firstName = request.getParameter("firstName");
        String secondName = request.getParameter("secondName");
        String gender = request.getParameter("gender");
        String language = request.getParameter("lang");

        if (login == null
                || login.length() < 1
                || password == null
                || password.length() < 1
                || gender == null
                || gender.length() < 1
                || firstName == null
                || firstName.length() < 1
                || secondName == null
                || secondName.length() < 1
                || language == null
                || language.length() < 1) {
            throw new AppException("Some required fields are empty");
        }

        try {

        UserService userService = ServiceFactory.getFactory(ServiceFactory.DEFAULT_SERVICE).getUserService();
        UserServiceEntity userServiceEntity = new UserServiceEntity();
        userServiceEntity.setLogin(login);
        userServiceEntity.setPassword(password);
        userServiceEntity.setRole(Role.User);
        userServiceEntity.setRegisterDate(new Date(System.currentTimeMillis()));
        userServiceEntity.setName(firstName, secondName, Locale.valueOf(language));

        userServiceEntity.setGender(Gender.valueOf(gender));

        userServiceEntity = userService.createUser(userServiceEntity);
        request.getSession().setAttribute("user", userServiceEntity);
        } catch (AppException e) {
            LOG.warn(e.getMessage(), e);
            ResourceBundle bundle = (ResourceBundle) request.getSession().getAttribute("resourceBundle");
            throw new AppException(bundle.getString("error.cannotRegister"));
        }

        return Path.PAGE_INDEX_PAGE;
    }
}
