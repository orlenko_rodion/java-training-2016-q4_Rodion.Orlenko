package ua.nure.orlenko.SummaryTask4.web.controller;

import org.apache.log4j.Logger;
import ua.nure.orlenko.SummaryTask4.web.Path;
import ua.nure.orlenko.SummaryTask4.web.controller.command.Command;
import ua.nure.orlenko.SummaryTask4.exception.AppException;
import ua.nure.orlenko.SummaryTask4.web.controller.command.concreteCommand.CommandContainer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *  Servlet controller
 */
public class ServletController extends javax.servlet.http.HttpServlet {

    private static final Logger LOG = Logger.getLogger(ServletController.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.debug("Controller processing");

        String commandName = request.getParameter("command");
        LOG.trace("Obtained commandName => " + commandName);

        Command command = CommandContainer.get(commandName);
        Command.putTuHistory(command);
        LOG.trace("Command obtained => " + command);

        String path = Path.PAGE_ERROR_PAGE;
        try {
            path = command.execute(request,response);
        } catch (AppException e) {
            request.setAttribute("errorMessage", e.getMessage());
        }

        LOG.trace("Forward address => " + path);

        request.getRequestDispatcher(path).forward(request,response);
    }
}
